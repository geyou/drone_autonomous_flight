#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <thread>

using namespace std;

#define SERVER_PORT htons(50007)

char client_message[2000];
int clientSock;
string command = "empty";

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

// The function we want to execute on the new thread.
void task(string msg)
{
        cout << "server thread task " << msg <<endl;
         char buffer[1000];
        int n;

        int serverSock=socket(AF_INET, SOCK_STREAM, 0);

        sockaddr_in serverAddr;
        serverAddr.sin_family = AF_INET;
        serverAddr.sin_port = SERVER_PORT;
        serverAddr.sin_addr.s_addr = INADDR_ANY;

        /* bind (this socket, local address, address length)
           bind server socket (serverSock) to server address (serverAddr).  
           Necessary so that server can use a specific port */ 
        bind(serverSock, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr));

        // wait for a client
        /* listen (this socket, request queue length) */
        listen(serverSock,5);
        sockaddr_in clientAddr;
        socklen_t sin_size=sizeof(struct sockaddr_in);
        clientSock=accept(serverSock,(struct sockaddr*)&clientAddr, &sin_size);
        
        cout << "Server en ecoute....:  "<< endl;
         bzero(buffer, 1000);
         while (read(clientSock, buffer, 500) ) {
                 command = buffer;
                 bzero(buffer, 1000);
         }
}


