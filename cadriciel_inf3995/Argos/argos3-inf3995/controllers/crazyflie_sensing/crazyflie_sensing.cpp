/* Include the controller definition */
#include "crazyflie_sensing.h"
/* Function definitions for XML parsing */
#include <argos3/core/utility/configuration/argos_configuration.h>
/* 2D vector definition */
#include <argos3/core/utility/math/vector2.h>
/* Logging */
#include <argos3/core/utility/logging/argos_log.h>
/* Threading */
#include <thread>
/* Server thread */
#include "serverTX.h"

/****************************************/
/****************************************/

thread t1(task, "running ...");

CCrazyflieSensing::CCrazyflieSensing() :
   m_pcDistance(NULL),
   m_pcPropellers(NULL),
   m_pcRNG(NULL),
   m_pcRABA(NULL),
   m_pcRABS(NULL),
   m_pcPos(NULL),
   m_pcBattery(NULL),
   m_uiCurrentStep(0) {}

/****************************************/
/****************************************/

void CCrazyflieSensing::Init(TConfigurationNode& t_node) {
   try {
      /*
       * Initialize sensors/actuators
       */
      m_pcDistance   = GetSensor  <CCI_CrazyflieDistanceScannerSensor>("crazyflie_distance_scanner");
      m_pcPropellers = GetActuator  <CCI_QuadRotorPositionActuator>("quadrotor_position");
      /* Get pointers to devices */
      m_pcRABA   = GetActuator<CCI_RangeAndBearingActuator>("range_and_bearing");
      m_pcRABS   = GetSensor  <CCI_RangeAndBearingSensor  >("range_and_bearing");
      try {
         m_pcPos = GetSensor  <CCI_PositioningSensor>("positioning");
      }
      catch(CARGoSException& ex) {}
      try {
         m_pcBattery = GetSensor<CCI_BatterySensor>("battery");
      }
      catch(CARGoSException& ex) {}      
   }
   catch(CARGoSException& ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initializing the crazyflie sensing controller for robot \"" << GetId() << "\"", ex);
   }
   /*
    * Initialize other stuff
    */
   /* Create a random number generator. We use the 'argos' category so
      that creation, reset, seeding and cleanup are managed by ARGoS. */
   m_pcRNG = CRandom::CreateRNG("argos");

   m_uiCurrentStep = 0;

   // Initialize XY position
   stepPos.push_back(m_pcPos->GetReading().Position.GetX());
   stepPos.push_back(m_pcPos->GetReading().Position.GetY());

   Reset();

   // FSM initial state
   currentState = MOVE_FORWARD;

   // Initialize rotation angles map with basic type CRadians
    rotationAnglesRad = { {1, 0.5 * CRadians::PI_OVER_SIX}, {2, CRadians::PI_OVER_SIX}, {3, CRadians::PI_OVER_FOUR}, {4, CRadians::PI_OVER_THREE}, 
    {5, 2.5 * CRadians::PI_OVER_SIX}, {6, CRadians::PI_OVER_TWO}, {7, 7 * CRadians::PI_OVER_SIX}, {8, 8 * CRadians::PI_OVER_SIX},
    {9, 9 * CRadians::PI_OVER_SIX}, {10, 10 * CRadians::PI_OVER_SIX}, {11, 11 * CRadians::PI_OVER_SIX}, {12, -0.5 * CRadians::PI_OVER_SIX},
    {13, -CRadians::PI_OVER_SIX}, {14, -CRadians::PI_OVER_FOUR}, {15, -CRadians::PI_OVER_THREE}, {16, -2.5 * CRadians::PI_OVER_SIX}, 
    {17, -CRadians::PI_OVER_TWO}, {18, -7 * CRadians::PI_OVER_SIX}, {19, -8 * CRadians::PI_OVER_SIX}, {20, -9 * CRadians::PI_OVER_SIX}, 
    {21, -10 * CRadians::PI_OVER_SIX}, {22, -11 * CRadians::PI_OVER_SIX} };
    // Initialize rotation angles map with basic type CDegrees
    rotationAnglesDeg = { {1, 15.0}, {2, 30.0}, {3, 45.0}, {4, 60.0}, {5, 75.0}, {6, 90.0}, {7, 105.0}, {8, 120.0}, {9, 135.0}, {10, 150.0},
    {11, 165.0}, {12, -15.0}, {13, -30.0}, {14, -45.0}, {15, -60.0}, {16, -75.0}, {17, -90.0}, {18, -105.0}, {19, -120.0}, {20, -135.0}, {21, -150.0},
    {22, -165.0} };

    // Initialize random number generator
    srand (time(NULL));

    dronePosition.open("data_pos.txt");
    rangingDeck_1.open("data_ranging_deck_1.txt");
    rangingDeck_2.open("data_ranging_deck_2.txt");
    rangingDeck_3.open("data_ranging_deck_3.txt");
    rangingDeck_4.open("data_ranging_deck_4.txt");
}

/****************************************/
/****************************************/

void CCrazyflieSensing::ControlStep() {

   Real X = m_pcPos->GetReading().Position.GetX();
   Real Y = m_pcPos->GetReading().Position.GetY();
   Real Z = m_pcPos->GetReading().Position.GetZ();
   Real yaw = acos(m_pcPos->GetReading().Orientation.GetZ()) * 360.0 / M_PI;


   // if (command != "TAKE_OFF" && command != "LAND" && command != "RETURN_TO_BASE") {
   //    currentState = WAITING_CMD;
   //    LOG << "Waiting state ..." << std::endl;
   // }

   // if (command == "TAKE_OFF") {
   //    currentState = MOVE_FORWARD;
   // }
   // if (command == "LAND") {
   //    currentState = LAND;
   // }
   // if (command == "RETURN_TO_BASE") {
   //    currentState = RETURN_TO_BASE;
   // }

   LOG << "Command: " << command << std::endl;


   LOG << "X = " << X << "\t" << "Y = " << Y << "\t" << "Z = " << Z << std::endl;
   LOG << "Yaw = " << yaw << std::endl;
   LOG << "Rotation angle = " << rotAngle << std::endl;

   Real frontAngle;
   if ((rotAngle <= 0 && rotAngle >= -90) || (rotAngle <= 180 && rotAngle > 90)) {
      frontAngle = yaw - 180;
   } else {
      frontAngle = 180 - yaw;
   }

    // Read distance sensor
   CCI_CrazyflieDistanceScannerSensor::TReadingsMap sDistRead = 
      m_pcDistance->GetReadingsMap();
   auto iterDistRead = sDistRead.begin();
   double front, left, back, right;
   // argos::CRadians front_ang, left_ang, back_ang, right_ang;
      right = (iterDistRead++)->second;
      LOG << "Right dist: " << right  << std::endl;
      front = (iterDistRead++)->second;
      LOG << "Front dist: "  << front << std::endl;
      left = (iterDistRead++)->second;
      LOG << "Left dist: "  << left  << std::endl;
      back = (iterDistRead)->second;
      LOG << "Back dist: " << back << std::endl;

   // Read battery level
   Real batteryLevel = m_pcBattery->GetReading().AvailableCharge;
   LOG << "Battery level = " << batteryLevel << std::endl;

   if (batteryLevel < 0.3) {
      command = "RETURN_TO_BASE";
   }

   // To select a random rotation angle
   int randNum = rand() % rotationAnglesDeg.size() + 1;

   // Set altitudes and get drone average speed every 10 timesteps
   if (m_uiCurrentStep % TIME_INTERVAL == 0) {
      getSpeed();
      
      if (m_strId == "drone1") {
      cPos.SetZ(ALTITUDE_1);
      rangingDeck_1 << X << "\t" << Y << "\t" << frontAngle << "\t" << front << "\t" << back << "\t" << left << "\t" << right << "\t" <<
      SPEED_X << "\t" << SPEED_Y << "\t" << batteryLevel << "\n";
      rangingDeck_1.flush();
   }

      if (m_strId == "drone2") {
      cPos.SetZ(ALTITUDE_2);
      rangingDeck_2 << "\t" << X << "\t" << Y << "\t" << frontAngle << "\t" << front << "\t" << back << "\t" << left << "\t" << right << "\t" <<
      SPEED_X << "\t" << SPEED_Y << "\t" << batteryLevel << "\n";
      rangingDeck_2.flush();
   }

      if (m_strId == "drone3") {
      cPos.SetZ(ALTITUDE_3);
      rangingDeck_3 << "\t" << X << "\t" << Y << "\t" << frontAngle << "\t" << front << "\t" << back << "\t" << left << "\t" << right << "\t" <<
      SPEED_X << "\t" << SPEED_Y << "\t" << batteryLevel << "\n";
      rangingDeck_3.flush();
   }

      if (m_strId == "drone4") {
      cPos.SetZ(ALTITUDE_4);
      rangingDeck_4 << "\t" << X << "\t" << Y << "\t" << frontAngle << "\t" << front << "\t" << back << "\t" << left << "\t" << right << "\t" <<
      SPEED_X << "\t" << SPEED_Y << "\t" << batteryLevel << "\n";
      rangingDeck_4.flush();
   }

      dronePosition << X << "\t" << Y << "\n";
      dronePosition.flush();
   }

  
   switch(currentState) {
      case MOVE_FORWARD:
      cPos.SetX(STEP);
      cPos.SetY(0.0);
      m_pcPropellers->SetRelativePosition(cPos);

      cPrevPos = m_pcPos->GetReading().Position;
      if (command == "LAND") {
      currentState = LAND;
   }

   if (command == "RETURN_TO_BASE") {
            currentState = RETURN_TO_BASE;
      }
   
      // Obstacle ahead
      if (front > 0) {
         currentState = DECELERATE;
      }
     
      break;
      case DECELERATE:
      cPrevPos = m_pcPos->GetReading().Position;
      if (command == "LAND") {
      currentState = LAND;
   }
      
      if (fabs(SPEED_X) > TOLERANCE || fabs(SPEED_Y) > TOLERANCE) {
         cPos.SetX(0.0);
      }
      if (fabs(SPEED_X) < TOLERANCE && fabs(SPEED_Y) < TOLERANCE) {
         cPos.SetX(STEP);
      }
      m_pcPropellers->SetRelativePosition(cPos);
      
      if (front < CLOSEST_DISTANCE_TO_OBSTACLE) {
         cPrevPos = m_pcPos->GetReading().Position;
         currentState = STABILIZE_TRANSLATION;
      }
      break;
      case ROTATE:
      cPrevPos = m_pcPos->GetReading().Position;
      if (command == "LAND") {
      currentState = LAND;
   }

      m_pcPropellers->SetAbsoluteYaw(rotationAnglesRad[randNum]);
      targetAngle = rotationAnglesDeg[randNum];
      rotAngle = rotationAnglesDeg[randNum];
      if (rotationAnglesDeg[randNum] < 0 && rotationAnglesDeg[randNum] >= -90) {
         targetAngle = 180 + rotationAnglesDeg[randNum];
      }
      else if (rotationAnglesDeg[randNum] <= 0 && rotationAnglesDeg[randNum] < -90) {
         targetAngle = -180 - 2 * rotationAnglesDeg[randNum];
      }
      else if (rotationAnglesDeg[randNum] > 0 && rotationAnglesDeg[randNum] <= 90) {
         targetAngle = 180 - rotationAnglesDeg[randNum];
      } else {
         targetAngle = -180 + 2 * rotationAnglesDeg[randNum];
      }
      currentState = STABILIZE_ROTATION;
      break;
      case STABILIZE_TRANSLATION:
      cPrevPos = m_pcPos->GetReading().Position;
      if (command == "LAND") {
      currentState = LAND;
   }

      cPos.SetX(cPrevPos.GetX());
      cPos.SetY(cPrevPos.GetY());
      m_pcPropellers->SetAbsolutePosition(cPos);
      Real diff;
      diff = fabs(m_pcPos->GetReading().Position.GetX() - cPrevPos.GetX());
  
      if (diff < TOLERANCE) {
            currentState = ROTATE;
      }
      break;
      case STABILIZE_ROTATION:
      cPrevPos = m_pcPos->GetReading().Position;
      if (command == "LAND") {
      currentState = LAND;
   }
      Real yaw;
      yaw = acos(m_pcPos->GetReading().Orientation.GetZ()) * 360.0 / M_PI;
      Real diffRot;
      diffRot = fabs(yaw - targetAngle);
      if (diffRot < TOLERANCE) {
         currentState = MOVE_FORWARD;
      }
      break;
      case LAND:
      cPos.SetX(cPrevPos.GetX());
      cPos.SetY(cPrevPos.GetY());
      cPos.SetZ(0.1);
      m_pcPropellers->SetAbsolutePosition(cPos);
      if (command == "TAKE_OFF") {
      currentState = MOVE_FORWARD;
      }
      else if (command == "RETURN_TO_BASE") {
      currentState = MOVE_FORWARD;
      } else {
      currentState = LAND;
      }
      break;
      case RETURN_TO_BASE:
      toBase();
      break;
      case WAITING_CMD:
      sleep(0.1);
      break;
      default:
      break;
   }
   
   m_uiCurrentStep++;

   // Close simulation window in case of bad command
   bool closeWindow = false;

   // Makes the main thread wait for the new thread to finish execution, therefore blocks its own execution.
   if(command != "TAKE_OFF" && command != "RETURN_TO_BASE" && command != "LAND" && closeWindow == true) {
      t1.join();
      LOG << "\nCApres le join" << std::endl;
      LOG << "\nClosing thread and conn" << std::endl;
      // close(clientSock);
   }

}

/****************************************/
/****************************************/

   void CCrazyflieSensing::getSpeed() {
      if (m_uiCurrentStep % TIME_INTERVAL == 0) {
         stepPos.push_back(m_pcPos->GetReading().Position.GetX());
         stepPos.push_back(m_pcPos->GetReading().Position.GetY());
      }
      // Retrieve X and Y positions to calculate the average drone speed
      SPEED_X = (stepPos.at(stepPos.size() - 2) - stepPos.at(stepPos.size() - 4)) / TIME_INTERVAL;
      SPEED_Y = (stepPos.back() - stepPos.at(stepPos.size() - 3)) / TIME_INTERVAL;

      LOG << "Speed_X = " << SPEED_X << "\t" << "Speed_Y = " << SPEED_Y << std::endl;
   }

   void CCrazyflieSensing::toBase() {
   Real X = m_pcPos->GetReading().Position.GetX();
   Real Y = m_pcPos->GetReading().Position.GetY();
   Real Z = m_pcPos->GetReading().Position.GetZ();

      if (m_strId == "drone1") {
         cPos.SetX(0.0);
         cPos.SetY(0.0);
         cPos.SetZ(0.1);
         m_pcPropellers->SetAbsolutePosition(cPos);
   }

      if (m_strId == "drone2") {
         cPos.SetX(0.2);
         cPos.SetY(0.0);
         cPos.SetZ(0.1);
         m_pcPropellers->SetAbsolutePosition(cPos);
   }

      if (m_strId == "drone3") {
         cPos.SetX(-0.2);
         cPos.SetY(0.0);
         cPos.SetZ(0.1);
         m_pcPropellers->SetAbsolutePosition(cPos);
   }

      if (m_strId == "drone4") {
         cPos.SetX(0.0);
         cPos.SetY(0.2);
         cPos.SetZ(0.1);
         m_pcPropellers->SetAbsolutePosition(cPos);
   }

   }

/****************************************/
/****************************************/

void CCrazyflieSensing::Reset() {
}


/****************************************/
/****************************************/

/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.
 * The string is then usable in the XML configuration file to refer to
 * this controller.
 * When ARGoS reads that string in the XML file, it knows which controller
 * class to instantiate.
 * See also the XML configuration files for an example of how this is used.
 */
REGISTER_CONTROLLER(CCrazyflieSensing, "crazyflie_sensing_controller")