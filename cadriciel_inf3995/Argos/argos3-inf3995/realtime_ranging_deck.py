import math
import json
import time
import requests

fo_1 = open("data_ranging_deck_1.txt", "r")
fo_2 = open("data_ranging_deck_2.txt", "r")
fo_3 = open("data_ranging_deck_3.txt", "r")
fo_4 = open("data_ranging_deck_4.txt", "r")

print("Sending simulation data to server ...")
url = 'http://localhost:5201'

while(True):
    line = fo_1.readline()
    time.sleep(0.001)
    if (line != ''):
        x = float(line.split()[0])
        y = float(line.split()[1])
        ang = float(line.split()[2])
        front = float(line.split()[3])/100 if float(line.split()[3]) > 0 else float(0)
        back = float(line.split()[4])/100 if float(line.split()[4]) > 0 else float(0)
        left = float(line.split()[5])/100 if float(line.split()[5]) > 0 else float(0)
        right = float(line.split()[6])/100 if float(line.split()[6]) > 0 else float(0)
        speedX = float(line.split()[7])
        speedY = float(line.split()[8])
        battery = float(line.split()[9])
        frontPosX = x + front * math.cos(math.radians(ang))
        frontPosY = y + front * math.sin(math.radians(ang))
        leftPosX = x + left * math.cos(math.radians(ang + 90))
        leftPosY = y + left * math.sin(math.radians(ang + 90))
        backPosX = x + back * math.cos(math.radians(ang + 180))
        backPosY = y + back * math.sin(math.radians(ang + 180))
        rightPosX = x + right * math.cos(math.radians(ang + 270))
        rightPosY = y + right * math.sin(math.radians(ang + 270))
        speed = math.sqrt(speedX**2 + speedY**2)
        obstacle_coordinates = json.dumps({'sim': 'sim', 'battery': battery, 'droneState': 0, 'droneNumber': 4, 'speed': speed, 'led': 6,
                                           'front': front, 'back': back, 'left': left, 'right': right, 'up': 0,
                                           'frontPosX': frontPosX, 'frontPosY': frontPosY,
                                           'leftPosX': leftPosX, 'leftPosY': leftPosY,
                                           'backPosX': backPosX, 'backPosY': backPosY, 
                                           'rightPosX': rightPosX, 'rightPosY': rightPosY
                                           })
        requests.post(url, data=obstacle_coordinates)

    line = fo_2.readline()
    if (line != ''):
        x = float(line.split()[0])
        y = float(line.split()[1])
        ang = float(line.split()[2])
        front = float(line.split()[3])/100 if float(line.split()[3]) > 0 else float(0)
        back = float(line.split()[4])/100 if float(line.split()[4]) > 0 else float(0)
        left = float(line.split()[5])/100 if float(line.split()[5]) > 0 else float(0)
        right = float(line.split()[6])/100 if float(line.split()[6]) > 0 else float(0)
        speedX = float(line.split()[7])
        speedY = float(line.split()[8])
        battery = float(line.split()[9])
        frontPosX = x + front * math.cos(math.radians(ang))
        frontPosY = y + front * math.sin(math.radians(ang))
        leftPosX = x + left * math.cos(math.radians(ang + 90))
        leftPosY = y + left * math.sin(math.radians(ang + 90))
        backPosX = x + back * math.cos(math.radians(ang + 180))
        backPosY = y + back * math.sin(math.radians(ang + 180))
        rightPosX = x + right * math.cos(math.radians(ang + 270))
        rightPosY = y + right * math.sin(math.radians(ang + 270))
        speed = math.sqrt(speedX**2 + speedY**2)
        obstacle_coordinates = json.dumps({'sim': 'sim', 'battery': battery, 'droneState': 0, 'droneNumber': 4, 'speed': speed, 'led': 6,
                                        'front': front, 'back': back, 'left': left, 'right': right, 'up': 0,
                                        'frontPosX': frontPosX, 'frontPosY': frontPosY,
                                        'leftPosX': leftPosX, 'leftPosY': leftPosY,
                                        'backPosX': backPosX, 'backPosY': backPosY, 
                                        'rightPosX': rightPosX, 'rightPosY': rightPosY
                                        })
        requests.post(url, data=obstacle_coordinates)

    line = fo_3.readline()
    if (line != ''):
        x = float(line.split()[0])
        y = float(line.split()[1])
        ang = float(line.split()[2])
        front = float(line.split()[3])/100 if float(line.split()[3]) > 0 else float(0)
        back = float(line.split()[4])/100 if float(line.split()[4]) > 0 else float(0)
        left = float(line.split()[5])/100 if float(line.split()[5]) > 0 else float(0)
        right = float(line.split()[6])/100 if float(line.split()[6]) > 0 else float(0)
        speedX = float(line.split()[7])
        speedY = float(line.split()[8])
        battery = float(line.split()[9])
        frontPosX = x + front * math.cos(math.radians(ang))
        frontPosY = y + front * math.sin(math.radians(ang))
        leftPosX = x + left * math.cos(math.radians(ang + 90))
        leftPosY = y + left * math.sin(math.radians(ang + 90))
        backPosX = x + back * math.cos(math.radians(ang + 180))
        backPosY = y + back * math.sin(math.radians(ang + 180))
        rightPosX = x + right * math.cos(math.radians(ang + 270))
        rightPosY = y + right * math.sin(math.radians(ang + 270))
        speed = math.sqrt(speedX**2 + speedY**2)
        obstacle_coordinates = json.dumps({'sim': 'sim', 'battery': battery, 'droneState': 0, 'droneNumber': 4, 'speed': speed, 'led': 6,
                                           'front': front, 'back': back, 'left': left, 'right': right, 'up': 0,
                                           'frontPosX': frontPosX, 'frontPosY': frontPosY,
                                           'leftPosX': leftPosX, 'leftPosY': leftPosY,
                                           'backPosX': backPosX, 'backPosY': backPosY, 
                                           'rightPosX': rightPosX, 'rightPosY': rightPosY
                                           })
        requests.post(url, data=obstacle_coordinates)

    line = fo_4.readline()
    if (line != ''):
        x = float(line.split()[0])
        y = float(line.split()[1])
        ang = float(line.split()[2])
        front = float(line.split()[3])/100 if float(line.split()[3]) > 0 else float(0)
        back = float(line.split()[4])/100 if float(line.split()[4]) > 0 else float(0)
        left = float(line.split()[5])/100 if float(line.split()[5]) > 0 else float(0)
        right = float(line.split()[6])/100 if float(line.split()[6]) > 0 else float(0)
        speedX = float(line.split()[7])
        speedY = float(line.split()[8])
        battery = float(line.split()[9])
        frontPosX = x + front * math.cos(math.radians(ang))
        frontPosY = y + front * math.sin(math.radians(ang))
        leftPosX = x + left * math.cos(math.radians(ang + 90))
        leftPosY = y + left * math.sin(math.radians(ang + 90))
        backPosX = x + back * math.cos(math.radians(ang + 180))
        backPosY = y + back * math.sin(math.radians(ang + 180))
        rightPosX = x + right * math.cos(math.radians(ang + 270))
        rightPosY = y + right * math.sin(math.radians(ang + 270))
        speed = math.sqrt(speedX**2 + speedY**2)
        obstacle_coordinates = json.dumps({'sim': 'sim', 'battery': battery, 'droneState': 0, 'droneNumber': 4, 'speed': speed, 'led': 6,
                                           'front': front, 'back': back, 'left': left, 'right': right, 'up': 0,
                                           'frontPosX': frontPosX, 'frontPosY': frontPosY,
                                           'leftPosX': leftPosX, 'leftPosY': leftPosY,
                                           'backPosX': backPosX, 'backPosY': backPosY, 
                                           'rightPosX': rightPosX, 'rightPosY': rightPosY
                                           })
        requests.post(url, data=obstacle_coordinates)

fo_1.close()
fo_2.close()
fo_3.close()
fo_4.close()