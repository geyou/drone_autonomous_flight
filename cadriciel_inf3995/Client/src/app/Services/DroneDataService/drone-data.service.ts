import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { promise } from 'protractor';
import { interval, Observable } from 'rxjs';
import { DroneData } from 'src/Models/DroneData/drone-data';
import { Point2D } from 'src/Models/Point2D';
import { Trajectory } from 'src/Models/Trajectory';


export interface DroneInterface{
drone: DroneData;
}
@Injectable({
  providedIn: 'root'
})
export class DroneDataService {
datas: DroneData[];
point2D: Point2D;
testPoint2D: Point2D;
trajectory: Trajectory;
droneMessage: string;
found = false;
urlServer = 'http://localhost:5201/drone1';
urlServer2 = 'http://localhost:5201/drone2';
globalPath: String[] = [];

// juste pour les test de la fonction simulation
k = 0;
l = 0;
c = 0;

  constructor(private http: HttpClient) {
      this.datas = [];
   }

   command(url: string, message: string, droneNumber: number): void{
    this.sendMessageToDrone(url, message, droneNumber);
   }
   StartDrone(url: string, message: string, droneNumber: number): void{
    this.sendMessageToDrone(url, message, droneNumber);
   }

   StopDrone(url: string, message: string, droneNumber: number): void{
    this.sendMessageToDrone(url, message, droneNumber);
   }

   TriggerLedDrone(url: string, message: string, droneNumber: number): void{
    this.sendMessageToDrone(url, message, droneNumber);
   }
// Drone 1
   onGet(url: string): void {
    console.log('DONNEES DE DRONES INSIDE ON GET');
    this.getItems(url);
  }

  getItems(url: string): void {
    console.log('DONNEES DE DRONES INSIDE GET ITEM');
    this.http.get<any[]>(url)
      .subscribe(
        drones => {
          console.log('DONNEES DE DRONES INSIDE SUBCRIBE');
          console.log('DONNEES DE DRONES' + (drones.length));
          for (let i = 0; i < (drones.length  ); i++ ){
            console.log('DONNEES DE DRONESssssssss' + (drones.length ));
            this.found = false;
            let data: DroneData;
            data = new DroneData(
              drones[i].droneInstance.name,
              drones[i].droneInstance.droneState,
              false,
              false,
              false,
              false,
              false,
              i+1,
              drones[i].droneInstance.front,
              drones[i].droneInstance.back,
              drones[i].droneInstance.left,
              drones[i].droneInstance.right,
              drones[i].droneInstance.up,
              drones[i].droneInstance.frontPosX,
              drones[i].droneInstance.frontPosY,
              drones[i].droneInstance.backPosX,
              drones[i].droneInstance.backPosY,
              drones[i].droneInstance.leftPosX,
              drones[i].droneInstance.leftPosY,
              drones[i].droneInstance.rightPosX,
              drones[i].droneInstance.rightPosY,
              drones[i].droneInstance.battery,
              drones[i].droneInstance.speed,
              ((drones[i].droneInstance.battery* 100) / 4.7)
            );


            console.log("datat iciiiii");
            console.log(data);
              

            for (let pos = 0; pos < this.datas.length; pos++){
              if ( this.datas[pos].droneNumber === data.droneNumber){
                console.log(this.datas);
                
                this.datas[pos] = data;
                let scale;
                if(data.name == "sim") 
                {
                   scale = 1/100;
                   if (this.datas[pos].front > 0){
                    this.validatePointFront(scale,this.datas[pos]);
                   }
                   if (this.datas[pos].back > 0){
                    this.validatePointBack(scale,this.datas[pos]);
                   }
                   if (this.datas[pos].left > 0){
                    this.validatePointLeft(scale,this.datas[pos]);
                   }
                   if (this.datas[pos].right > 0){
                    this.validatePointRight(scale,this.datas[pos]);
                   }

                }else {
                  console.log ("SCALEEE")
                  scale = 40;

                  if (this.datas[pos].frontPosX >= 0 ){
                    this.validatePointFront(scale,this.datas[pos]);
                   }
                  /*if (this.datas[pos].backPosX >= 0 ){
                    this.validatePointBack(scale,this.datas[pos]);
                   }*/
                  if (this.datas[pos].leftPosX >= 0 ){
                    this.validatePointLeft(scale,this.datas[pos]);
                   }
                  /*if (this.datas[pos].rightPosX >= 0 ){
                    this.validatePointRight(scale,this.datas[pos]);
                   }*/
                }

                /*if (this.datas[pos].front > 0 || this.datas[pos].front > 0) {
                let posx = Math.floor(this.datas[pos].frontPosX/scale);
                let posy = Math.floor(this.datas[pos].frontPosY/scale);
                this.addTrajectory(new Point2D(posx,posy), this.datas[pos]);
                console.log(" position front iciiiiiiiiiiiiiiiiiiiiiiiiiiiii");
                console.log(posx);
                console.log(posy);
                } */            

               /* if (this.datas[pos].back > 0 || this.datas[pos].back < 1000 || true) {
                let posx = Math.floor(this.datas[pos].backPosX/scale);
                let posy = Math.floor(this.datas[pos].backPosY/scale);
                this.addTrajectory(new Point2D(posx,posy), this.datas[pos]);
                }

                if (this.datas[pos].left > 0 || this.datas[pos].left < 1000 || true) {
                let posx = Math.floor(this.datas[pos].leftPosX/scale);
                let posy = Math.floor(this.datas[pos].leftPosY/scale);
                this.addTrajectory(new Point2D(posx,posy), this.datas[pos]);
                }

                if (this.datas[pos].right > 0 || this.datas[pos].right < 1000 || true) {
                let posx = Math.floor(this.datas[pos].rightPosX/scale);
                let posy = Math.floor(this.datas[pos].rightPosY/scale);
                this.addTrajectory(new Point2D(posx,posy), this.datas[pos]);
                }
*/

                //this.SimulationParcour(data);
                this.found = true;
              }
              if (this.found) { break; }
            }

            if (!this.found){
              this.datas.push(data);
            }
          }

    /*this.http.get<any[]>(url)
      .subscribe(
        data => {
          data.forEach(drone => {
            this.data.battery = drone.battery;
            this.data.droneState = drone.droneState;
            this.data.droneNumber = drone.droneNumber;
            this.data.speed = drone.speed;
            this.data.led1 = drone.led1;
            this.data.led2 = drone.led2;
            this.data.led3 = drone.led3;
            this.data.led4 = drone.led4;
            // this.data.map = drone.map;
            this.addTrajectory(drone.point);
            console.log(this.data.battery);
            this.datas.push(this.data);
          });*/
        }
      );
  }

  addTrajectory( point: any, data: DroneData): void{
    if(data.name == "sim"){
      this.point2D = new Point2D(point.y, point.x); 
    }else{
      this.point2D = new Point2D(point.x, point.y);
    }
    data.map.Trajectories.tabPoint.push(this.point2D);
    this.toString(this.point2D, data);
  
  }

  sendMessageToDrone(url: string, message: string, droneNumber: number ): void{
    this.http
      .post(url, message)
      .subscribe(
        () => {
          console.log('Message envoye au drone est: ' + message);
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
}

toString(pt: Point2D, data: DroneData): void {
 
  if (data.name == 'drone') {
  const height = 100;//Math.floor(document.getElementById("zoneDessin").getBoundingClientRect().height / 2);
  const width = 100;//Math.floor(document.getElementById("zoneDessin").getBoundingClientRect().width / 2);
  const point :Point2D = {x: pt.x + width , y: pt.y + height};
  console.log(height);
  console.log(width);

  const radius = 3;
  const diam = radius * 2;
  const pt1 = point.x - radius;
 
  this.globalPath.push(`M ${pt1} ${point.y} ` + ` a ${radius}, ${radius} 0 1, 0 ${diam}, 0 a ${radius}, ${radius} 0 1, 0 -${diam}, 0`);
  }
  
 
/********* Pour Gergi ****************/
 if(data.name == "sim"){
  const height = Math.floor(document.getElementById("zoneDessin").getBoundingClientRect().height / 2);
  const width = Math.floor(document.getElementById("zoneDessin").getBoundingClientRect().width / 2);
  const point :Point2D = {x: pt.x + width , y: pt.y + height};
  console.log(height);
  console.log(width);

  const radius = 3;
  const diam = radius * 2;
  const pt1 = point.x - radius;

  console.log("pppppppppppppppoooooooooooooiiiinnnnntttttt  xxxxxxxx " + point.x);

  this.globalPath.push(`M ${pt1} ${point.y} ` + ` a ${radius}, ${radius} 0 1, 0 ${diam}, 0 a ${radius}, ${radius} 0 1, 0 -${diam}, 0`);
  }
  console.log(data.map.Trajectories.path);
}

// Pour les tests
async Simulation(): Promise<void>{
  let data: DroneData;
  data = new DroneData(
              "simu",
              1,
              false,
              false,
              false,
              false,
              false,
              2,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              ((0 * 100) / 4.7),
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0
            );
  this.datas.push(data);
  this.SimulationParcour(data);
}

async SimulationParcour(data: DroneData): Promise<void>{
  if(this.c <= 3){
    setTimeout(() =>
         {
          if(this.c === 0){
            this.k = 30;
            this.l+=30;
            this.testPoint2D = new Point2D(this.k, this.l);
            if(this.l >= 600){this.c++; this.l = 600;}
          }
          if(this.c === 1){
            this.l = 600;
            this.k+=30;
            this.testPoint2D = new Point2D(this.k, this.l);
            if(this.k >= 510){this.c++; this.k = 510;}
          }
          if(this.c === 2){
            this.k = 510;
            this.l-=30;
            this.testPoint2D = new Point2D(this.k, this.l);
            if(this.l <= 30){this.c++; this.l = 30;}
          }
          if(this.c === 3){
            this.l = 30;
            this.k-=30;
            this.testPoint2D = new Point2D(this.k, this.l);
            if(this.k <= 30){this.c++; this.l = 30;}
          }
          this.addTrajectory(this.testPoint2D, data);
           this.SimulationParcour(data);
        },
        1000);
  }
}

validatePointFront (scale: number, data: DroneData): void{
  let posx = Math.floor(data.frontPosX/scale);
  let posy = Math.floor(data.frontPosY/scale);
  this.addTrajectory(new Point2D(posx,posy), data);
  console.log(" position front iciiiiiiiiiiiiiiiiiiiiiiiiiiiii");
  console.log(posx);
  console.log(posy);
}

validatePointBack (scale: number, data: DroneData): void{
  let posx = Math.floor(data.backPosX/scale);
  let posy = Math.floor(data.backPosY/scale);
  this.addTrajectory(new Point2D(posx,posy), data);
  console.log(" position back iciiiiiiiiiiiiiiiiiiiiiiiiiiiii");
  console.log(posx);
  console.log(posy);
}

validatePointLeft (scale: number, data: DroneData): void{
  let posx = Math.floor(data.leftPosX/scale);
  let posy = Math.floor(data.leftPosY/scale);
  this.addTrajectory(new Point2D(posx,posy), data);
  console.log(" position left iciiiiiiiiiiiiiiiiiiiiiiiiiiiii");
  console.log(posx);
  console.log(posy);
}

validatePointRight (scale: number, data: DroneData): void{
  let posx = Math.floor(data.rightPosX/scale);
  let posy = Math.floor(data.rightPosY/scale);
  this.addTrajectory(new Point2D(posx,posy), data);
  console.log(" position right iciiiiiiiiiiiiiiiiiiiiiiiiiiiii");
  console.log(posx);
  console.log(posy);
}
}
