import { Injectable } from '@angular/core';
import { DroneData } from 'src/Models/DroneData/drone-data';
import { Map } from 'src/Models/Map';
import { DroneDataService } from '../DroneDataService/drone-data.service';

@Injectable({
  providedIn: 'root'
})
export class DrawMapService {
drones: DroneData[];
  constructor(
    public control: DroneDataService
  ) {
    this.drones = this.control.datas;
   }

}
