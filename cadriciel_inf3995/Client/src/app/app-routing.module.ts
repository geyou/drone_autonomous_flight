import { NgModule } from '@angular/core';
import {  RouterModule, Routes } from '@angular/router';
import { LedComponent } from './Components/led/led.component';
import { MapComponent } from './Components/map/map.component';
import { UserViewComponent } from './Components/user-view/user-view.component';

// routes
const routes: Routes = [
    { path: 'Logs', component: UserViewComponent },
    { path: 'Drones', component: LedComponent},
    { path: 'Map', component: MapComponent},
    { path: '', redirectTo: 'Drones', pathMatch: 'full' },
    { path: '**', redirectTo: 'Drones', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
