import { Component, OnInit } from '@angular/core';
import { DrawMapService } from 'src/app/Services/DrawMapService/draw-map.service';
import { DroneDataService } from 'src/app/Services/DroneDataService/drone-data.service';
import { DroneData } from 'src/Models/DroneData/drone-data';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

width: number;
height: number;
url = 'http://localhost:5201';

  constructor(
    public control: DroneDataService,
    public  mapping: DrawMapService
  ) {
    // this.drones = this.control.datas;
    this.width = 1250;
    this.height = 650 ;
  }

  ngOnInit(): void {
  }

  command(commande:string):void{
    switch(commande)
    {
      case "TAKE_OFF" :
        for (let i = 0 ; i < this.control.datas.length; i++){
          this.control.command( this.url, commande, this.control.datas[i].droneNumber);
        }
        
        break;
      case "RETURN_TO_BASE":
        for (let i = 0 ; i < this.control.datas.length; i++){
          this.control.command( this.url, commande, this.control.datas[i].droneNumber);
        }
        break;
      case "LAND":
        for (let i = 0 ; i < this.control.datas.length; i++){
          this.control.command( this.url, commande, this.control.datas[i].droneNumber);
        }
        break;
      case "UPDATE":
        for (let i = 0 ; i < this.control.datas.length; i++){
          this.control.command( this.url, commande, this.control.datas[i].droneNumber);
        }
        break;

    }
  }

}
