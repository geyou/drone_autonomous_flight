import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { DroneDataService } from 'src/app/Services/DroneDataService/drone-data.service';
import { DroneData } from 'src/Models/DroneData/drone-data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'INF3995-Equipe104';
  url = 'http://localhost:5201';
  url1 = 'http://localhost:5201/drone1';
  url2 = 'http://localhost:5201/drone2';
  ON = 'ON';
  OFF = 'OFF';
  repeat: any;

  constructor(
    public control: DroneDataService
  ){
    this.repeat = interval(200);
  }

  ngOnInit(): void{
    this.repeat.subscribe(() =>
    {
    this.GetBatteryLevels();
    });
  }

 /* Drone1On(): void{
    this.control.StartDrone(this.url1, this.ON,  this.drone1.droneNumber);
    this.drone1.droneState = this.control.data.droneState;
  }

  Drone2ON(): void{
    this.control.StartDrone(this.url2, this.ON, this.drone1.droneNumber);
    this.drone2.droneState = this.control.data.droneState;
  }

  Drone1OFF(): void{
    this.control.StartDrone(this.url1, this.OFF, this.drone2.droneNumber);
    this.drone1.droneState = this.control.data.droneState;
  }

  Drone2OFF(): void{
    this.control.StartDrone(this.url1, this.OFF, this.drone2.droneNumber);
    this.drone2.droneState = this.control.data.droneState;
  }*/

  GetBatteryLevels(): void{ // va etre appeler dans un fonction recursive
      console.log('DONNEES DE DRONEeeeeeeeeeeeeeeeeeeee');
      this.control.onGet(this.url);
      //this.GetBatteryPecentage();
  }

  GetBatteryPecentage(): void{
    this.control.datas.forEach(drone => {
      drone.batteryPercentage = ((drone.battery * 100) / 4.7);
    });
  }

  /*GetAllDrones(): void{
    //this.drones = this.control.datas;
    // Pour les test
    //this.control.datas.push(this.drone1);
    //this.control.datas.push(this.drone2);
  }*/
}
