import { Component, OnInit } from '@angular/core';
import { DroneDataService } from 'src/app/Services/DroneDataService/drone-data.service';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {

  constructor(
    public control: DroneDataService
  ) { }

  ngOnInit(): void {
  }

}
