import { Component, OnInit } from '@angular/core';
import { DroneDataService } from 'src/app/Services/DroneDataService/drone-data.service';
import { DroneData } from 'src/Models/DroneData/drone-data';

@Component({
  selector: 'app-led',
  templateUrl: './led.component.html',
  styleUrls: ['./led.component.scss']
})
export class LedComponent  {
drone: DroneData;
url1 = 'http://localhost:5201/drone1';
url2 = 'http://localhost:5201/drone2';
url = 'http://localhost:5201';

  constructor(
    public control: DroneDataService
  ) {
  }

  droneLedOn_Off(message: string, droneNumber: number): void{
    console.log('numero du drone' + droneNumber);

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.control.datas.length; i++) {
      if (this.control.datas[i].droneNumber === droneNumber){
        this.drone = this.control.datas[i];
      }
    }

    this.control.TriggerLedDrone(this.url, message, droneNumber);

    switch (message)
    {
      case('LED1_ON') : {
        this.drone.led1 = true; // this.control.data.led1;
        break;
      }
      case('LED2_ON') : {
        this.drone.led2 = true; // this.control.data.led2;
        break;
      }
      case('LED3_ON') : {
        this.drone.led3 = true; // this.control.data.led3;
        break;
      }
      case('LED4_ON') : {
        this.drone.led4 = true; // this.control.data.led4;
        break;

      }
      case('LEDA_ON') : {
        this.drone.led1 = true; // this.control.data.led4;
        this.drone.led2 = true;
        this.drone.led3 = true;
        this.drone.led4 = true;
        this.drone.ledA = true;
        break;
      }

      case('LED1_OFF') : {
        this.drone.led1 = false; // this.control.data.led1;
        break;
      }

      case('LED2_OFF') : {
        this.drone.led2 = false; // this.control.data.led2;
        break;
      }

      case('LED3_OFF') : {
        this.drone.led3 = false; // this.control.data.led3;
        break;
      }

      case('LED4_OFF') : {
        this.drone.led4 = false; // this.control.data.led4;
        break;
      }

      case('LEDA_OFF') : {
        this.drone.ledA = false; // this.control.data.led4;
        this.drone.led1 = false; // this.control.data.led4;
        this.drone.led2 = false;
        this.drone.led3 = false;
        this.drone.led4 = false;
        break;
      }
      default:
        break;

    }
  }

}
