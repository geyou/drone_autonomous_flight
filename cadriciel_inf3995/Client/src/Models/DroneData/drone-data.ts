import { Map } from '../Map';
import { Trajectory } from '../Trajectory';


enum droneStates {STANDBY , IN_MISSION, CRASHED}

export class DroneData {
  name: string;
  droneNumber: number;
  battery: number;
  batteryPercentage: number;
  droneState: number;
  state: string;
  speed: number;
  led1: boolean;
  led2: boolean;
  led3: boolean;
  led4: boolean;
  ledA: boolean;
  front: number;
  back: number;
  left: number;
  right: number;
  up: number;
  map: Map;
  frontPosX : number;
  frontPosY : number;
  backPosX : number;
  backPosY : number;
  leftPosX : number;
  leftPosY : number;
  rightPosX : number;
  rightPosY : number;

  constructor(
    name:string,
    droneState: number,
    led1: boolean,
    led2: boolean,
    led3: boolean,
    led4: boolean,
    ledA: boolean,
    droneNumber: number,
    front: number,
    back: number,
    left: number,
    right: number,
    up: number,
    frontPosX : number,
    frontPosY : number,
    backPosX : number,
    backPosY : number,
    leftPosX : number,
    leftPosY : number,
    rightPosX : number,
    rightPosY : number,
    battery?: number,
    speed?: number,
    batteryPercentage?: number
    ) {
    this.name = name;
    this.battery = battery;
    this.droneState = droneState;
    if (this.droneState === 0) {
      this.state = 'STANDBY';
    }else if (this.droneState === 1){
      this.state = 'IN_MISSION';
    }else if (this.droneState === 2){
      this.state = 'CRASHED';
    }else{
      this.state = 'UNKNOWN';
    }
    this.droneNumber = droneNumber;
    this.batteryPercentage = batteryPercentage;
    this.speed = speed;
    this.led1 = led1;
    this.led2 = led2;
    this.led3 = led3;
    this.led4 = led4;
    this.ledA = ledA;
    this.front = front;
    this.back = back;
    this.left = left;
    this.right = right;
    this.up = up;
    this.frontPosX = frontPosX;
    this.frontPosY = frontPosY;
    this.backPosX = backPosX;
    this.backPosY = backPosY;
    this.leftPosX = leftPosX;
    this.leftPosY = leftPosY;
    this.rightPosX = rightPosX;
    this.rightPosY = rightPosY;
    // Pour le Test
    this.map = new Map();
   }
}
