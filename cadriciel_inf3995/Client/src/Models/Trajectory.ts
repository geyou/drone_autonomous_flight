import { Point2D } from './Point2D';

export class Trajectory {
    tabPoint: Point2D[];
    path: string[];
    id: number;
    constructor() {
        this.path = [];
        this.tabPoint = [];
}
}
