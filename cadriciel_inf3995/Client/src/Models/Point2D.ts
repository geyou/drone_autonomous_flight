export class Point2D {
    x: number;
    y: number;

    static add = (point1: Point2D, point2: Point2D): Point2D => {
        return new Point2D(point1.x + point2.x, point1.y + point2.y);
    }

    static sub = (point1: Point2D, point2: Point2D): Point2D => {
        return new Point2D(point1.x - point2.x, point1.y - point2.y);
    }
    static div = (point1: Point2D, div: number): Point2D => {
        return new Point2D(point1.x / div, point1.y / div);
    }
    static equal = (point1: Point2D, point2: Point2D): boolean => {
        return (point1.x === point2.x) && (point1.y === point2.y);
    }

    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }
}
