
import enum 

class DroneDatas: 
  
  def __init__(self, droneNumber):
    self.name = "drone"
    self.battery = 0
    self.droneState = 0
    self.droneNumber = droneNumber
    self.speed = 0
    self.led = 6
    self.front = 0
    self.back = 0
    self.left = 0
    self.right = 0
    self.up = 0
    self.frontPosX = 0.0
    self.frontPosY = 0.0
    self.backPosX = 0.0
    self.backPosY = 0.0
    self.leftPosX = 0.0
    self.leftPosY = 0.0
    self.rightPosX = 0.0
    self.rightPosY = 0.0

  def dump(self):
    return {"droneInstance": {'name': self.name,
                          'battery': self.battery,
                          'droneState': self.droneState,
                          'droneNumber': self.droneNumber,
                          'speed': self.speed,
                          'led': self.led,
                          'front': self.front,
                          'back': self.back,
                          'left': self.left,
                          'right': self.right,
                          'up': self.up,
                          'frontPosX':self.frontPosX,
                          'frontPosY':self.frontPosY,
                          'backPosX': self.backPosX,
                          'backPosY': self.backPosY,
                          'leftPosX': self.leftPosX,
                          'leftPosY': self.leftPosY,
                          'rightPosX': self.rightPosX,
                          'rightPosY': self.rightPosY}}


        
