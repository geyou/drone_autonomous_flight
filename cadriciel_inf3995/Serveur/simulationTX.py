import time
from threading import Thread

import requests, json

import struct



import socket
import sys
import time

import LEDsModel as leds



class SimulationTX:
    """Example that connects to a Crazyflie and ramps the motors up/down and
    the disconnects"""

    datas = 0
    running = 1
    HOST, PORT = "localhost", 50007
    link = socket.socket(socket.AF_INET, socket.SOCK_STREAM)



    def _connection_init(self):
   
        print('Connecting to simulation...')
         
        self.link.connect((self.HOST, self.PORT)) 
        print("Connected to the simulation")
        

    def _app_packet_send(self, data):
        print("sending request")

        ###############################################################
        # For testing purposes only. Delete this line in final release
        # self.link.send(bytes(data, 'utf-8')) # Send cmd to simulator
        # print("cmdSim: ", data)
        ###############################################################

        if data == b'LAND':
            self.link.send(b'LAND')
        
        if data == b'RETURN_TO_BASE':
            self.link.send(b'RETURN_TO_BASE')
        
        if data == b'TAKE_OFF':
            self.link.send(b'TAKE_OFF')

    def _app_packet_received(self):
        print("Step 2: receiving")
        while self.running == 1 :
            if str(self.link.recv(1000)):
                self.datas = str(self.link.recv(1000))
                print(self.datas)

        
       # print("receive" + str(self.link.recv(1000)))

    def init_connection(self):
        Thread(target=self._connection_init(self)).start()
        #Thread(target=self._app_packet_received(self)).start()
        #self._connection_init(self)

    def start_command(self, data):
        self._app_packet_send(self, data)

        # self._app_packet_received(self)
        #Thread(target=self._app_packet_received(self)).start()
