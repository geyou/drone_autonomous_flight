# -*- coding: utf-8 -*-
#
#     ||          ____  _ __
#  +------+      / __ )(_) /_______________ _____  ___
#  | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
#  +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
#   ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
#
#  Copyright (C) 2014 Bitcraze AB
#
#  Crazyflie Nano Quadcopter Client
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA  02110-1301, USA.
"""
Simple example that connects to the first Crazyflie found, Sends and
receive appchannel packets

The protocol is:
 - 3 floats are send, x, y and z
 - The Crazyflie sends back the sum as one float
"""
import logging
import datetime
import time
from threading import Thread

import requests, json

import struct
import subprocess
import math


import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.mem import MemoryElement

import LEDsModel as leds
import droneDatas as drone

logging.basicConfig(level=logging.ERROR)

class NotConnected(RuntimeError):
    pass


class SendReceivePacket:
    """Example that connects to a Crazyflie and ramps the motors up/down and
    the disconnects"""

    batteryLevel = 0
    drones = list()
   

    def __init__(self, link_uri):
        """ Initialize and run the example with the specified link_uri """

        self._cf = Crazyflie()

        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        self._cf.appchannel.packet_received.add_callback(self._app_packet_received)

        self._cf.open_link(link_uri)
        #print('initialise connecting var')
        self.connected = True

        print('Connecting to %s' % link_uri)


    def _connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""
        self.connected = True
        print('Packet send start  ')
        
       
        
    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the specified address)"""
        print('Connection to %s failed: %s' % (link_uri, msg))
        self.connected = False

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print('Connection to %s lost: %s' % (link_uri, msg))
        self.connected = False

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print('Disconnected from %s' % link_uri)
        self.connected = False





    def _app_packet_received(self, data):
        self.drones[0].name = "drone"
        arg1,arg2,arg3,arg4,arg5,arg6 = struct.unpack("<ffffff", data)
        if arg6 == 1.0:
            self.drones[0].state = arg1
            self.drones[0].battery = arg2
            self.drones[0].speed = arg3
            self.drones[0].LedsOn = arg4
            self.drones[0].droneNumber = arg5
            """print(f" si packet DRONE1 STATE")
            print(f"state: {arg1}")
            print(f"battery: {arg2}")
            print(f"speed: {arg3}")
            print(f"LedsOn: {arg4}")
            print(f"droneNumber: {arg5}")"""
            #print(f"parity: {arg6}")
        elif arg6 == 0.0:
            self.drones[0].front = arg1
            self.drones[0].back = arg2
            self.drones[0].left = arg3
            self.drones[0].right = arg4
            self.drones[0].up = arg5
            """print(f" si packet DRONE1 Ranging")
            print(f"front: {arg1}")
            print(f"back: {arg2}")
            print(f"left: {arg3}")
            print(f"right: {arg4}")"""
            #print(f"up: {arg5}")
            #print(f"parity: {arg6}")
        elif arg6 == 7.0:
            tempx = float(arg1*1000 + self.drones[0].front * math.cos(math.radians(arg5)))
            tempy = float(arg2*1000 +  self.drones[0].front * math.sin(math.radians(arg5)))

            if(tempx <= 10000.0 and tempy <= 10000.0 and tempx >= -10000.0 and tempy >= -10000.0):
                self.drones[0].frontPosX = 10000 - tempy
                self.drones[0].frontPosY = 10000 - tempx
                #print(f"front pos ytttttttttttttttttttttttttttttttttttTTTTTTTTTTTTTTTTTTTT: { arg1}")
                #print(f" front pos xTttttttttttttttttTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTY: { arg2}")
            else:
                self.drones[0].frontPosX = -1
                self.drones[0].frontPosY = -1
            #self.drones[0].frontPosX = arg1*10 + self.drones[0].front * math.cos(math.radians(arg5))
            #self.drones[0].frontPosY = arg2*10 +  self.drones[0].front * math.sin(math.radians(arg5))

            tempx = arg1*10000 + self.drones[0].left * math.cos(math.radians(arg5 + 90))
            tempy = arg2*10000 + self.drones[0].left * math.cos(math.radians(arg5 + 90))

            if(tempx <= 10000.0 and tempy <= 10000.0 and  tempx >= -10000.0 and tempy >= -10000.0):
                self.drones[0].leftPosX = 10000 - tempy
                self.drones[0].leftPosY = 10000 - tempx
            else:
                self.drones[0].leftPosX = -1
                self.drones[0].leftPosY = -1
            #self.drones[0].leftPosX = arg1*10 + self.drones[0].left * math.cos(math.radians(arg5 + 90))
            #self.drones[0].leftPosY = arg2*10 + self.drones[0].left * math.cos(math.radians(arg5 + 90))

            tempx = arg1*10000 + self.drones[0].back * math.cos(math.radians(arg5 + 180))
            tempy = arg2*10000 + self.drones[0].back * math.cos(math.radians(arg5 + 180))

            if(tempx <= 10000 and tempy <= 10000 and tempx >= -10000.0 and tempy >= -10000.0):
                self.drones[0].backPosX = 10000 - tempy
                self.drones[0].backPosY = 10000 - tempx
            else:
                self.drones[0].backPosX = -1
                self.drones[0].backPosY = -1
            #self.drones[0].backPosX = arg1*10 + self.drones[0].back * math.cos(math.radians(arg5 + 180))
            #self.drones[0].backPosY = arg2*10 + self.drones[0].back * math.cos(math.radians(arg5 + 180))

            tempx = arg1*10000 + self.drones[0].right * math.cos(math.radians(arg5 + 270))
            tempy = arg2*10000 + self.drones[0].right * math.cos(math.radians(arg5 + 270))

            if(tempx <= 10000 and tempy <= 10000 and tempx >= -10000.0 and tempy >= -10000.0):
                self.drones[0].rightPosX = 10000 - tempy
                self.drones[0].rightPosY = 10000 - tempx
            else:
                self.drones[0].rightPosX = -1
                self.drones[0].rightPosY = -1
            #self.drones[0].rightPosX = arg1*10 + self.drones[0].right * math.cos(math.radians(arg5 + 270))
            #self.drones[0].rightPosY = arg2*10 + self.drones[0].right * math.cos(math.radians(arg5 + 270))

            self.drones[0].up = arg5
            """print(f" si packet DRONE1 POSsssssssssssssssssssssssssssssss")
            print(f"GetPosX: {arg1}")
            print(f"GetPosY: {arg2}")
            print(f"angle: {arg3}")
            print(f"angle: {arg4}")
            print(f"yaw: {arg5}")"""
        elif arg6 == 2.0:
            self.drones[0].frontPosX = -arg2
            self.drones[0].frontPosY = arg1
            self.drones[0].backPosX = -arg4
            self.drones[0].backPosY = arg3
            self.drones[0].up = arg5
            """print(f" si packet DRONE1 ")
            print(f"frontPosX: {arg1}")
            print(f"frontPosY: {arg2}")"""
            #print(f"backPosX: {arg3}")
            #print(f"backPosY: {arg4}")
            #print(f"backPosY: {arg5}")
            #print(f"parity: {arg6}") """
        elif arg6 == 3.0:
            self.drones[0].leftPosX = -arg2
            self.drones[0].leftPosY = arg1
            self.drones[0].rightPosX = -arg4
            self.drones[0].rightPosY = arg3
            self.drones[0].up = arg5
            #print(f" si packet RANGING")
            #print(f"leftPosX: {arg1}")
            #print(f"leftPosY: {arg2}")
            #print(f"rightPosX: {arg3}")
            #print(f"rightPosY: {arg4}")
            #print(f"rightPosY: {arg5}")
            #print(f"parity: {arg6}")"""
        elif arg6 == 4.0:
            """print(f" si packet DRONE2 POSssssssssssssssssssssss")
            print(f"PosX: {arg1}")
            print(f"PosY: {arg2}")
            print(f"fpos: {arg3}")
            print(f"fpos: {arg4}")
            print(f"yaw: {arg5}")"""
            #print(f"angle: {arg5}")
            #print(f"rightPosY: {arg5}")
            #print(f"parity: {arg6}")"""
        elif arg6 == 5.0:
            """print(f" si packet DRONE2 Rang")
            print(f"front: {arg1}")
            print(f"back: {arg2}")
            print(f"left: {arg3}")
            print(f"right: {arg4}")"""
        elif arg6 == 6.0:
            self.drones[0].state = arg1
            self.drones[0].battery = arg2
            self.drones[0].speed = arg3
            self.drones[0].LedsOn = arg4
            self.drones[0].droneNumber = arg5
            print(f" si packet DRONE2 STATE")
            print(f"state: {arg1}")
            print(f"battery: {arg2}") 
            print(f"speed: {arg3}")
            print(f"LedsOn: {arg4}")
            print(f"droneNumber: {arg5}")
            #print(f"parity: {arg6}")
            
           


    def app_packet_send(self, LedsOn, command):
        #for i in range(2):
        print(LedsOn)
       
        data = struct.pack("<fI", float(LedsOn), command)
    
        self._cf.appchannel.send_packet(data)
        print(f"command send: {LedsOn}")
        print(f"command send: {command}")
          
        # self._cf.close_link()



    def _test_appchannel(self):
        for i in range(10):
            (x, y, z) = (i, i+1, i+2)
            data = struct.pack("<fff", x, y, z)
            self._cf.appchannel.send_packet(data)
            print(f"Sent x: {x}, y: {y}, z: {z}")

            time.sleep(1)
        self._cf.close_link()


    def start_command(self, data):
        print(f"data value: {data}")
        if data == b'LEDA_ON':
            Thread(target=self.app_packet_send(leds.Leds.LED_ALL.value,0 )).start()    
        if data == b'LEDA_OFF':
            Thread(target= self.app_packet_send(leds.Leds.LED_ALL_OFF.value, 0)).start()
        if data == b'LED1_ON':
            Thread(target= self.app_packet_send(leds.Leds.LED_BLUE_L, 0)).start()
        if data == b'TAKE_OFF':
            print(f"OOOOOOOOONNNNN EEEESSSTTT  DDDAAAANNNSS  TTTAAAKKEEOOOOFFFF")
            Thread(target= self.app_packet_send( -1.0, 1)).start()
        if data == b'UPDATE':
            #if self.drones[0].battery >= 1.41 and self.drones[0].droneState == 0.0:
                #Thread(target= self.app_packet_send( -1.0, 2)).start()
            subprocess.call(['./srcipt.sh'])
            #else ajouter un champ dans la structure pour indiquer batterie faible
            

        if data == b'LAND':
            Thread(target= self.app_packet_send(-1.0, 3)).start()
        if data == b'RETURN_TO_BASE':
            Thread(target= self.app_packet_send(-1.0, 4)).start()



    def search_memories(self):
        """
        Search and return list of 1-wire memories.
        """
        print('search for memory')
        if  not self.connected:
            raise NotConnected()
        print(self._cf.mem.get_mems(MemoryElement.TYPE_1W))
        return self._cf.mem.get_mems(MemoryElement.TYPE_1W)


    def wait_for_connection(self, timeout=10):
        """
        Busy loop until connection is established.
        Will abort after timeout (seconds). Return value is a boolean, whether
        connection could be established.
        """
        start_time = datetime.datetime.now()
        print('Wait for connection')
        while True:
            if self.connected:
                return True
            now = datetime.datetime.now()
            if (now - start_time).total_seconds() > timeout:
                return False
            time.sleep(1)