import enum 

class Leds(enum.Enum): 
        LED_BLUE_L = 0
        LED_GREEN_L = 1 
        LED_RED_L =2
        LED_GREEN_R = 3 
        LED_RED_R = 4
        LED_ALL = 5
        LED_ALL_OFF = 6
