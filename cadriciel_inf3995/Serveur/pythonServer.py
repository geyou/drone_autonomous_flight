from http.server import HTTPServer, BaseHTTPRequestHandler
import json


import sendReceivePacket as communication
import logging
import time
import os
from threading import Thread

import requests, json

import struct

import cflib
from cflib.crazyflie import Crazyflie

import LEDsModel as leds
import simulationTX
import droneDatas as drone

import socket
import sys
import subprocess


simDrones =list()



class Server(BaseHTTPRequestHandler):
    droneTestSim = []
 
    def do_GET(self):
        #print('AFFICHAGE SERVEUR')
        
        if sys.argv[1] == "drone":
            json_string = json.dumps([drone.dump() for drone in droneConnect.drones])
            #print('Tailllllleee deeeee AVAAAIIILLAABBBLLLEEE', len(droneConnect.drones))
        elif sys.argv[1] == "simulation":
            json_string = json.dumps([drone.dump() for drone in simDrones])
        #print('json_string', json_string) 
        try:
            self.send_response(200)
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write(json_string.encode())
        except:
            self.send_response(404)
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write(json_string.encode())
        
    
    def do_POST(self):
        #print("content TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTJOIJFOISEJOIEWF:")
        print("incomming http: ", self.path)
        # <--- Gets the size of data
        content_length = int(self.headers['Content-Length'])
        # <--- Gets the data itself
        post_data = self.rfile.read(content_length)
        print("content TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTJOIJFOISEJOIEWF: %s" % post_data)

        if post_data != b'UPDATE' and post_data != b'LAND' and post_data != b'RETURN_TO_BASE' and post_data != b'TAKE_OFF' and post_data != b'LEDA_ON' and post_data != b'LEDA_OFF':
            datas = json.loads(post_data) 
            get_argos_datas(datas)
        
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        if sys.argv[1] == "simulation":  
            sim.start_command(sim, post_data)
        elif sys.argv[1] == "drone" :
            droneConnect.start_command(post_data)
            #droneConnect.start_command(b'UPDATE')
            #le.start_command(b'UPDATE')
        

def get_argos_datas(simData):
    simDrones[0].name = simData["sim"]
    simDrones[0].battery = simData["battery"]
    simDrones[0].droneState = simData["droneState"]
    simDrones[0].droneNumber = simData["droneNumber"]
    simDrones[0].speed = simData["speed"]
    simDrones[0].led = simData["led"]
    simDrones[0].front =  simData["front"]
    simDrones[0].back = simData["back"]
    simDrones[0].right = simData["right"] 
    simDrones[0].left =  simData["left"]
    simDrones[0].up =  simData["up"]
    simDrones[0].frontPosX = simData["frontPosX"]
    simDrones[0].frontPosY = simData["frontPosY"]
    simDrones[0].backPosX =  simData["backPosX"]
    simDrones[0].backPosY = simData["backPosX"]
    simDrones[0].leftPosX = simData["leftPosX"]
    simDrones[0].leftPosY = simData["leftPosY"]
    simDrones[0].rightPosX = simData["rightPosX"]
    simDrones[0].rightPosY = simData["rightPosY"]
    print('json_string', simDrones[0].battery)
    print('json_string',  simDrones[0].rightPosY)



    

def connection_init():
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    # Scan for Crazyflies and use the first one found
    print('Scanning interfaces for Crazyflies...')
    available = cflib.crtp.scan_interfaces()
    print('Crazyflies found:')
    
    for i in available:
        #radioLink = i[0]
        print(i[0])

    if len(available) > 0:
        global droneConnect
        droneConnect = communication.SendReceivePacket(available[0][0])
    else:
        print('No Crazyflies found, cannot run example')
    
    for i in available:
        if "usb" not in i[0]:
            droneConnect.drones.append(drone.DroneDatas(i))

    
def main():

    port = 5201
    httpd = HTTPServer(('localhost', port), Server)
    simDrones.append(drone.DroneDatas(0))


    if len( sys.argv ) == 1:
        print( "Add an argument to run the server" )
        exit()
  
    print( sys.argv[1] )

    if sys.argv[1] == "drone":  
        connection_init()
   
    elif sys.argv[1] == "simulation":
        global sim 
        sim = simulationTX.SimulationTX
        sim.init_connection(sim)
        simDrones.append(drone.DroneDatas(0))

        ###############################################################
        # For testing purposes only. Delete this line in final release
        # sim.start_command(sim, "TAKE_OFF") # Valid cmd
        # sim.start_command(sim, "START_MISSION") # Bad cmd
        ###############################################################

        ###############################################################
        # For testing purposes only. Delete this line in final release
        # sim.start_command(sim, "LAND") # Valid cmd
        # sim.start_command(sim, "START_MISSION") # Bad cmd
        ###############################################################

    print(f"Server listening on port {port}")
    httpd.serve_forever()

   
    


if __name__ == "__main__":
    

    main()



