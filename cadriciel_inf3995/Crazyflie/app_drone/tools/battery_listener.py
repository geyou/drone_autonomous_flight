import logging
import time

import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie

from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.syncLogger import SyncLogger

# Only output errors from the logging framework
logging.basicConfig(level=logging.ERROR)

def log_battery_callback(timestamp, data, logconf):
    batteryLevel = data["pm.batteryLevel"]
    print('[%d][%s]: %s' % (timestamp, logconf.name, data))
    return batteryLevel

# Asynchronous communication
def simple_log_async(scf, logconf):
    cf = scf.cf
    cf.log.add_config(logconf)
    logconf.data_received_cb.add_callback(log_battery_callback)
    logconf.start()
    time.sleep(10)
    logconf.stop()

# Synchronous communication
# def simple_log(scf, logconf):
#     with SyncLogger(scf, logconf) as logger:
#         for log_entry in logger:
#             timestamp = log_entry[0]
#             data = log_entry[1]
#             logconf_name = log_entry[2]
#             print('[%d][%s]: %s' % (timestamp, logconf_name, data))
#             break

def test_connection():
    print("Yeah, I'm connected! :D")
    time.sleep(3)
    print("Now I will disconnect :'(")


if __name__ == '__main__':
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    available = cflib.crtp.scan_interfaces()
    
    for i in available:
        print(i[0])

    if len(available) > 0:
        uri = available[0][0]
        lg_battery = LogConfig(name='pm', period_in_ms=500)
        lg_battery.add_variable('pm.batteryLevel', 'float')

        with SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache')) as scf:
            simple_log_async(scf, lg_battery)
            # simple_log(scf, lg_battery)
    else:
        print('No Crazyflies found, cannot run example')


    
    
