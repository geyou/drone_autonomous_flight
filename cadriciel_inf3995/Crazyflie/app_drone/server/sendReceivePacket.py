# -*- coding: utf-8 -*-
#
#     ||          ____  _ __
#  +------+      / __ )(_) /_______________ _____  ___
#  | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
#  +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
#   ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
#
#  Copyright (C) 2014 Bitcraze AB
#
#  Crazyflie Nano Quadcopter Client
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA  02110-1301, USA.
"""
Simple example that connects to the first Crazyflie found, Sends and
receive appchannel packets

The protocol is:
 - 3 floats are send, x, y and z
 - The Crazyflie sends back the sum as one float
"""
import logging
import time
from threading import Thread

import requests, json

import struct

import cflib
from cflib.crazyflie import Crazyflie

import LEDsModel as leds

logging.basicConfig(level=logging.ERROR)



class SendReceivePacket:
    """Example that connects to a Crazyflie and ramps the motors up/down and
    the disconnects"""

    batteryLevel = 0

    def __init__(self, link_uri):
        """ Initialize and run the example with the specified link_uri """

        self._cf = Crazyflie()

        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        self._cf.appchannel.packet_received.add_callback(self._app_packet_received)

        self._cf.open_link(link_uri)


        print('Connecting to %s' % link_uri)


    def _connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""

        print('Packet send start  ')
       
        
    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the specified address)"""
        print('Connection to %s failed: %s' % (link_uri, msg))

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print('Connection to %s lost: %s' % (link_uri, msg))

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print('Disconnected from %s' % link_uri)





    def _app_packet_received(self, data):
        #(voltageLevel,voltageLevelMin,voltageLevelMax, ) = struct.unpack("<fff", data)
        #print(f"baterie level: {voltageLevel}")
        #print(f"baterie level Min: {voltageLevelMin}")
        #print(f"baterie level Max: {voltageLevelMax}")
        #self.batteryLevel = voltageLevel
        (front,back,left,right,) = struct.unpack("<ffff", data)
        print(f"front: {front}")
        print(f"back: {back}")
        print(f"left: {left}")
        print(f"right: {right}")
        


    def app_packet_send(self, LedsOn):
        #for i in range(2):
        print(LedsOn)
        data = struct.pack("<f", float(LedsOn))
        self._cf.appchannel.send_packet(data)
        print(f"command send: {LedsOn}")
          
        # self._cf.close_link()



    def _test_appchannel(self):
        for i in range(10):
            (x, y, z) = (i, i+1, i+2)
            data = struct.pack("<fff", x, y, z)
            self._cf.appchannel.send_packet(data)
            print(f"Sent x: {x}, y: {y}, z: {z}")

            time.sleep(1)

        self._cf.close_link()


    def start_command(self, data):
        print(f"data value: {data}")
        if data == b'ON':
            Thread(target=self.app_packet_send(leds.Leds.LED_ALL.value)).start()
            
        if data == b'OFF':
            Thread(target= self.app_packet_send(leds.Leds.LED_ALL_OFF.value)).start()


