from http.server import HTTPServer, BaseHTTPRequestHandler
import json
# from aptdaemon import client

import sendReceivePacket as communication
import logging
import time
from threading import Thread

import requests, json

import struct

import cflib
from cflib.crazyflie import Crazyflie

import LEDsModel as leds


class Server(BaseHTTPRequestHandler):


      
    def do_GET(self):
    
        batteryLevel = le.batteryLevel
        print("battery niveau:", batteryLevel )
        if self.path == '/drone1':
            items = [{'droneName' : 'CrazyFly1', 'battery': batteryLevel,
                      'position' : 'Left',  'state': 'ON'}]
        if self.path == '/drone2':
            items = [{'droneName' : 'CrazyFly2', 'battery': 100,
                      'position' : 'Right',  'state': 'ON'}]

        try:
            self.send_response(200)
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write(json.dumps(items).encode())
        except:
            self.send_response(404)
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()
            self.wfile.write(json.dumps(items).encode())
    
    def do_POST(self):

       
        print("incomming http: ", self.path)
        # <--- Gets the size of data
        content_length = int(self.headers['Content-Length'])
        # <--- Gets the data itself
        post_data = self.rfile.read(content_length)
        print("content : %s" % post_data)
        self.send_response(200)
        le.start_command(post_data)
        

def connection_init():
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    # Scan for Crazyflies and use the first one found
    print('Scanning interfaces for Crazyflies...')
    available = cflib.crtp.scan_interfaces()
    print('Crazyflies found:')
    for i in available:
        print(i[0])

    if len(available) > 0:
        global le
        le = communication.SendReceivePacket(available[0][0])
    else:
        print('No Crazyflies found, cannot run example')


def main():
    port = 5201
    httpd = HTTPServer(('localhost', port), Server)
    connection_init()
    print(f"Server listening on port {port}")
    httpd.serve_forever()
   
    


if __name__ == "__main__":

    main()



