/**
 * exemple d'utilisation du logging
 *https://www.bitcraze.io/documentation/repository/crazyflie-firmware/master/userguides/logparam/
 *
**/
#include "app.h"
#include "app_channel.h"
#include "log.h"

#include "debug.h"

#define DEBUG_MODULE "HELLOWORLD"

struct testPacketTX {
  float batteryLevel;
} __attribute__((packed));

void appMain()
{
  DEBUG_PRINT("Waiting for activation ...\n");

  struct testPacketTX txPacket;

  while(1) {

    logVarId_t idBatteryLevel = logGetVarId("pm", "batteryLevel");
    txPacket.batteryLevel = logGetFloat(idBatteryLevel);

    appchannelSendPacket(&txPacket, sizeof(txPacket));
    
  }

}
