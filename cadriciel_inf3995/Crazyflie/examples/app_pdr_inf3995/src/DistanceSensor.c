#include "droneFile.h"
#include "range.h"




paramVarId_t initMultiRangerDeck(){
    return paramGetVarId("deck", "bcMultiranger");
}

void logGetVariablesId(){
    idUp = logGetVarId("range", "up");
    idLeft = logGetVarId("range", "left");
    idRight = logGetVarId("range", "right");
    idFront = logGetVarId("range", "front");
    idBack = logGetVarId("range", "back");
}


void getDistances(){
    left = rangeGet( rangeLeft);
    right = rangeGet( rangeRight);
    front = rangeGet( rangeFront);
    back = rangeGet( rangeBack);
    up = rangeGet( rangeUp);
}

void getDistancesFromLogs(){
    left = logGetUint(idLeft);
    right = logGetUint(idRight);
    front = logGetUint(idFront);
    back = logGetUint(idBack);
    up = logGetUint(idUp);
}