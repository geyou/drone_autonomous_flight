// rangeGet(rangeDirection_t direction)
   
    // temps -- ;
    // vTaskDelay(M2T(5000));

    /*ledSetAll();
    vTaskDelay(M2T(5000));
    ledClearAll();
    vTaskDelay(M2T(5000));*/
    //height += 0.0f;
    /*temps += 1;
    if( temps < 5000){
      setHoverSetpoint(&setpoint, velFront, velSide, height, 0, 2.0f);
      commanderSetSetpoint(&setpoint, 3);
    } else {
        setHoverSetpoint(&setpoint, 0.0f, 0.0f, 0.0f, 0, 2.0f);
         commanderSetSetpoint(&setpoint, 3);
        temps = 0;
    }*/


    static setpoint_t setpoint;

  float velFront;
float height;
float velSide;
int temps;


velFront = 0.0f;
  velSide = 0.0f;
  height = 0.0f;
  // temps = 20000;
  setHoverSetpoint(&setpoint, velFront, velSide, height, 0, 0.0f);
  //commanderSetSetpoint(&setpoint, 3);

 static void setHoverSetpoint(setpoint_t *setpoint, float vx, float vy, float z, float yawrate, float thrust)
{
  setpoint->mode.z = modeAbs;
  setpoint->position.z = z;

  setpoint->thrust = thrust;

  setpoint->mode.yaw = modeVelocity;
  setpoint->attitudeRate.yaw = yawrate;


  setpoint->mode.x = modeVelocity;
  setpoint->velocity.x = vx;

  setpoint->mode.y = modeVelocity;
  setpoint->velocity.y = vy;

  setpoint->velocity_body = true;
}