#ifndef __DRONEFILE_H__
#define __DRONEFILE_H__

#include <stdbool.h>
#include <stdint.h>

#include "log.h"
#include "param.h"

// LOG Variable for id of direction
logVarId_t idUp;
logVarId_t idLeft;
logVarId_t idRight;
logVarId_t idBack;
logVarId_t idFront;

// Distance to a 
float front;
float back;
float left;
float right;
float up;

int ledNumber;
// Fonction pour la led
void setLeds(int ledNumber);


//Fonction pour la Batterie
float getBatteryLevel();


//Fonctions pour le multi-ranging deck
paramVarId_t initMultiRangerDeck();
void logGetVariablesId();
void getDistances();
void getDistances();
void getDistancesFromLogs();


//Fonctions pour le flow deck

#endif
