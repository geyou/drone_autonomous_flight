/**
 * ,---------,       ____  _ __
 * |  ,-^-,  |      / __ )(_) /_______________ _____  ___
 * | (  O  ) |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * | / ,--´  |    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *    +------`   /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie control firmware
 *
 * Copyright (C) 2019 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * appchanel_test.c: Demonstrate the appchanel functionality 
 */


#include "app.h"
#include "pm.h"
#include "app_channel.h"
#include "led.h"

#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "app.h"

#include "droneFile.h"

#include "commander.h"
#include "range.h"

#include "FreeRTOS.h"
#include "task.h"

#include "debug.h"

#include "log.h"
#include "param.h"



#define DEBUG_MODULE "HELLOWORLD"

typedef enum {STANDBY = 0, IN_MISSION, CRASHED } droneState;


struct testPacketRX {
  float LedsOn;
} __attribute__((packed));

struct rangingPacketTX {
  float front;
  float back;
  float left;
  float right;
  float up;
  int parity;
} __attribute__((packed));

struct droneDatasTX {
  float state;
  float battery;
  float speed;
  float LedsOn;
  float droneNumber;
  int parity;
} __attribute__((packed));



void appMain() {

  DEBUG_PRINT("Waiting for activation ...\n");

 // paramVarId_t idPositioningDeck = 
  paramVarId_t idMultiranger = initMultiRangerDeck();

  struct rangingPacketTX txPacketRanging;
  struct droneDatasTX txPacketDatas;
  struct testPacketRX rxPacket; 

  logGetVariablesId();

  while(true) {

    // uint8_t positioningInit = paramGetUint(idPositioningDeck);
    uint8_t multirangerInit = paramGetUint(idMultiranger);

    getDistances();

   if( multirangerInit) {

    txPacketDatas.state = STANDBY;
    txPacketDatas.battery = getBatteryLevel();
    txPacketDatas.speed = 5.0;
    txPacketDatas.LedsOn = ledNumber;
    txPacketDatas.droneNumber = 1;
    txPacketDatas.parity= 1;

    txPacketRanging.front= front;
    txPacketRanging.back = back;
    txPacketRanging.left = left;
    txPacketRanging.right= right;
    txPacketRanging.up = up;
    txPacketRanging.parity= 0;
   
    }

    appchannelSendPacket(&txPacketRanging, sizeof(txPacketRanging)); 
    appchannelSendPacket(&txPacketDatas, sizeof(txPacketDatas));
    vTaskDelay(M2T(1000));
   

    if (appchannelReceivePacket(&rxPacket, sizeof(rxPacket), 0)) {

      DEBUG_PRINT("App channel received Set Led ON Led: %d\n", (int)rxPacket.LedsOn);
      int led = (int)rxPacket.LedsOn;
      setLeds(led);
      
      
    }
  }

}

