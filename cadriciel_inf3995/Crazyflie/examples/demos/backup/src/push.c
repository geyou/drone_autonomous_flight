#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "app.h"
#include "led.h"

#include "commander.h"

#include "FreeRTOS.h"
#include "task.h"

#include "debug.h"

#include "log.h"
#include "param.h"
#include "droneLib.h"

#include "app_channel.h"

#include "estimator_kalman.h"

#include "pm.h"
#include "sitaw.h"


#define DEBUG_MODULE "PUSH"

/*static void setHoverSetpoint(setpoint_t *setpoint, float vx, float vy, float z, float yawrate)
{
  setpoint->mode.z = modeAbs;
  setpoint->position.z = z;


  setpoint->mode.yaw = modeVelocity;
  setpoint->attitudeRate.yaw = yawrate;


  setpoint->mode.x = modeVelocity;
  setpoint->mode.y = modeVelocity;
  setpoint->velocity.x = vx;
  setpoint->velocity.y = vy;

  setpoint->velocity_body = true;
}*/

static State state = idle;

bool idlePass = false;

// wall detection  variables
bool detectLeft = false;
bool detectRight = false;
bool detectFront = false;
bool detectBack = false;
//bool detectUp = false;

bool turn = false;
bool droneTakeoff = false;
int position;
float velX;
float velY;

int stateDrone = 0;

void appMain()
{
  static setpoint_t setpoint;
  static point_t setPosition;

/*********** Code Serveur ***************/
   
struct droneDatasTX txPacketDatas;
struct rangingPacketTX txPacketRanging;
struct testPacketRX rxPacket; 
struct sensorPositionFB positionFB;
struct sensorPositionLR positionLR;
/*********** *********** ***************/

  vTaskDelay(M2T(20));

  logVarId_t idUp = logGetVarId("range", "up");
  logVarId_t idLeft = logGetVarId("range", "left");
  logVarId_t idRight = logGetVarId("range", "right");
  logVarId_t idFront = logGetVarId("range", "front");
  logVarId_t idBack = logGetVarId("range", "back");

  
  
  paramVarId_t idPositioningDeck = paramGetVarId("deck", "bcFlow2");
  paramVarId_t idMultiranger = paramGetVarId("deck", "bcMultiranger");



  float factor = velMax/lowRadius;

 uint16_t up_o;

  while(1)
  {

    vTaskDelay(M2T(10));

    uint16_t up = logGetUint(idUp);
    uint16_t left = logGetUint(idLeft);
    uint16_t right = logGetUint(idRight);
    uint16_t front = logGetUint(idFront);
    uint16_t back = logGetUint(idBack);

    uint8_t positioningInit = paramGetUint(idPositioningDeck);
    uint8_t multirangerInit = paramGetUint(idMultiranger);
    estimatorKalmanGetEstimatedRot(anglePos);
    estimatorKalmanGetEstimatedPos(&setPosition);

    if( multirangerInit && positioningInit) {

      txPacketRanging.front= front;
      txPacketRanging.back = back;
      txPacketRanging.left = left;
      txPacketRanging.right= right;
      txPacketRanging.up = up;
      txPacketRanging.parity= 0;

      txPacketDatas.state = stateDrone;
      txPacketDatas.battery = pmGetBatteryVoltage();
      txPacketDatas.speed = sqrt((velFront* velFront) + (velSide* velSide));
      txPacketDatas.LedsOn = ledNumber;
      txPacketDatas.droneNumber = -1;
      txPacketDatas.parity= 1;



      positionFB.frontPosX = setPosition.x + (float)front * (anglePos[0]);
      positionFB.frontPosY = setPosition.y + (float)front * (anglePos[3]);

      positionFB.backPosX = setPosition.x + (float)back * ((anglePos[0] * (float)cos(90)) - (anglePos[3] * (float)sin(90)));
      positionFB.backPosY = setPosition.y + (float)back * ((anglePos[3] * (float)cos(90)) + (anglePos[0] * (float)sin(90)));
      positionFB.backPosYF = setPosition.y + (float)back * ((anglePos[3] * (float)cos(90)) + (anglePos[0] * (float)sin(90)));

      positionFB.parity = 2;

      positionLR.leftPosX = setPosition.x + (float)left * ((anglePos[0] * (float)cos(180)) - (anglePos[3] * (float)sin(180)));
      positionLR.leftPosY = setPosition.y + (float)left * ((anglePos[3] * (float)cos(180)) + (anglePos[0] * (float)sin(180)));

      positionLR.rightPosX = setPosition.x + (float)right * ((anglePos[0] * (float)cos(270)) - (anglePos[3] * (float)sin(270)));
      positionLR.rightPosY = setPosition.y + (float)right * ((anglePos[3] * (float)cos(270)) + (anglePos[0] * (float)sin(270)));
      positionLR.rightPosYF = setPosition.y + (float)right * ((anglePos[3] * (float)cos(270)) + (anglePos[0] * (float)sin(270)));
    
      positionLR.parity = 3;
    }

    appchannelSendPacket(&txPacketRanging, sizeof(txPacketRanging));
    appchannelSendPacket(&txPacketDatas, sizeof(txPacketDatas)); 
    appchannelSendPacket(&positionFB, sizeof(positionFB)); 
    appchannelSendPacket(&positionLR, sizeof(positionLR)); 
    //vTaskDelay(M2T(1000));
   

    if (appchannelReceivePacket(&rxPacket, sizeof(rxPacket), 0)) {

      DEBUG_PRINT("App channel received Set Led ON Led: %d\n", (int)rxPacket.LedsOn);
      //int led = (int)rxPacket.LedsOn;
      int command = (int)rxPacket.command;
      //setLeds(led);

      // appeller la machine a etat avec command
      if(command == 1){
          txPacketRanging.front = 10000.0;
          appchannelSendPacket(&txPacketRanging, sizeof(txPacketRanging)); 
          droneTakeoff = true;
    
      }
      
    }
 // DEBUT DU CODE DE TAKEOFF
   
   uint16_t left_o = lowRadius - MIN(left, lowRadius);
   uint16_t right_o = lowRadius - MIN(right, lowRadius);
   float l_comp = (-1) * left_o * factor;
   float r_comp = right_o * factor;
   velSide = r_comp + l_comp;

   uint16_t front_o = lowRadius - MIN(front, lowRadius);
   uint16_t back_o = lowRadius - MIN(back, lowRadius);
   float f_comp = (-1) * front_o * factor;
   float b_comp = back_o * factor;
   velFront = b_comp + f_comp;

   
    detectLeft = (left < radius);
    detectRight = (right < radius);
    detectFront = (front < radius);
    detectBack = (back < radius);

    up_o = radius - MIN(up, radius);
    height = height_sp - up_o/1000.0f;

    if (up < unlockThLow && up > 0.001f && idlePass) {
        DEBUG_PRINT("Waiting for hand to be removed!\n");
        idlePass = false;
        state = stopping ;
    }
  
    if(sitAwTuDetected()){
      state = crashed;
    }

    switch (state)
    {
    case idle:
      memset(&setpoint, 0, sizeof(setpoint_t));
      commanderSetSetpoint(&setpoint, 3);
      //if (up < unlockThLow && up > 0.001f) {
        if (droneTakeoff) {
        DEBUG_PRINT("Waiting for hand to be removed!\n");
        //state = lowTakeOff;
        state = takeOff;
      }
      
      break;

    case lowTakeOff:
       if (up > unlockThHigh) {
           DEBUG_PRINT("Unlocked!\n");
            idlePass = true;
           state = takeOff;
        }
        
    break;

    case takeOff :
     
      // setHoverSetpoint(&setpoint, 0, 0, height,0);
      //commanderSetSetpoint(&setpoint, 3);
      stateDrone = 1;
      state = findWall;
      
    break;
    case stopping :
       memset(&setpoint, 0, sizeof(setpoint_t));
       commanderSetSetpoint(&setpoint, 3);
       droneTakeoff = false;
       state = idle;
    break; 

    // trip startpoint
    case findWall:
      if(!detectLeft){
        moveLeft(&setpoint);
      }else{
        if(detectLeft){
          moveInit(&setpoint);
         // moveLeft(&setpoint);
          state = forward;
         //ledClearAll();
         
          //vTaskDelay(M2T(2000));
          
        }
        
      } 
     
    break;

    case forward:
    if(!detectFront && detectLeft){
        moveForward(&setpoint);
      }else{

        if(detectFront){
          moveInit(&setpoint);
          state = rotateRight;
        }else{

          if(!detectFront && !detectLeft){
            //state = stopping;
              moveInit(&setpoint);
            state = lost;
          }
          else{
            state = lost;
          }
        }
        
      }
    //foundwall = false;
    //turn = false;
    break;

    case rotateRight :
    turnRight(&setpoint);
    if(!detectFront){
        moveInit(&setpoint);
        state = forward;
       //state = findWall;
    }
   
    break;

    case lost :
     //moveTo(&setpoint, 100);
     //vTaskDelay(M2T(20));
     //rotateWithAngle(&setpoint, 65.0f);  
     // vTaskDelay(M2T(20));
     //moveInit(&setpoint);
      //state = findWall;
      position = rand()%12;
      rotateWithAngle(&setpoint, tabAngle[position]);  
      vTaskDelay(M2T(50));
      state = findWall;

    break;
    
    case crashed :
      stateDrone = 2;
      state = stopping;

    break;

    case returnToBase :
    velX = (setPosition.x < 0)? stepX : (setPosition.x  = 0)? 0.0f : -stepX;
    velY = (setPosition.y < 0)? stepY : (setPosition.x  = 0)? 0.0f : -stepY;
    vTaskDelay(M2T(20));
    setHoverSetpoint(setpoint, velX, velY, height, 0);
    commanderSetSetpoint(setpoint, 3);
      
    break;

    default:
      break;
    }

  }
}