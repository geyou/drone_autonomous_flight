#ifndef __DRONELIB_H__
#define __DRONELIB_H__

#include "app.h"
#include "led.h"

typedef enum {
    idle,
    lowUnlock,
    unlocked,
    stopping,
    forward,
    moveback,
    rotateRight,
    rotateLeft,
    lowTakeOff,
    takeOff,
    findWall,
    lost,
    lostRotateLeft,
    crashed,
    returnToBase

} State;

/************************ Code Serveur **********************/
typedef enum {STANDBY = 0, IN_MISSION, CRASHED } droneState;


struct testPacketRX {
  float LedsOn;
  int command;
} __attribute__((packed));

struct rangingPacketTX {
  float front;
  float back;
  float left;
  float right;
  float up;
  int parity;
} __attribute__((packed));

struct droneDatasTX {
  float state;
  float battery;
  float speed;
  float LedsOn;
  float droneNumber;
  int parity;
} __attribute__((packed));

struct position {
  float x;
  float y;
} __attribute__((packed));


struct sensorPositionFB {

  float frontPosX;
  float frontPosY;
  float backPosX;
  float backPosY;
  float backPosYF;
  
  int parity;
} __attribute__((packed));

struct sensorPositionLR {
  
  float leftPosX;
  float leftPosY;
  float rightPosX;
  float rightPosY;
  float rightPosYF;

  int parity;
} __attribute__((packed));


/*********************************************************/

#define MAX(a,b) ((a>b)?a:b)
#define MIN(a,b) ((a<b)?a:b)

static const uint16_t unlockThLow = 100;
static const uint16_t unlockThHigh = 300;
static const uint16_t stoppedTh = 500;

static const float velMax = 1.0f;
static const uint16_t radius = 300;
static const uint16_t lowRadius = 150;

static const float height_sp = 0.2f;

static const float stepX = 0.1f;
static const float stepY = 0.1f;
static const float angle = 15.0f;

static const float tabAngle[12] = {45,-45,60,-60,90,-90,120,-120,150,-150,180,-180};

static float height;
static float velFront = 0.0f; 
static float velSide = 0.0f;

static float anglePos[9];

static int ledNumber = 6;
// static struct rangingPacketTX txPacketRanging;


//static float maxAngle;


// Fonction pour faire avancer le drone


static void setHoverSetpoint(setpoint_t *setpoint, float vx, float vy, float z, float yawrate)
{
  setpoint->mode.z = modeAbs;
  setpoint->position.z = z;


  setpoint->mode.yaw = modeVelocity;
  setpoint->attitudeRate.yaw = yawrate;


  setpoint->mode.x = modeVelocity;
  setpoint->mode.y = modeVelocity;
  setpoint->velocity.x = vx;
  setpoint->velocity.y = vy;

  setpoint->velocity_body = true;
}

void moveLeft(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, 0, stepY, height, 0);
    commanderSetSetpoint(setpoint, 3);
    
}

void turnRight(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, velFront, velSide, height, -angle);
    commanderSetSetpoint(setpoint, 3);
}

void turnLeft(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, velFront, velSide, height, angle);
    commanderSetSetpoint(setpoint, 3);
    //vTaskDelay(M2T(20));
}

void moveForward(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, stepX, velSide, height, 0);
    commanderSetSetpoint(setpoint, 3);
    //vTaskDelay(M2T(20));
}

void rotateWithAngle(setpoint_t *setpoint, float rotateAngle){
    setHoverSetpoint(setpoint, velFront, velSide, height, rotateAngle);
    commanderSetSetpoint(setpoint, 3);
    //vTaskDelay(M2T(20));
}

void moveInit(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, 0, 0, height, 0);
    commanderSetSetpoint(setpoint, 3);
    //vTaskDelay(M2T(20));
}

void moveTo(setpoint_t *setpoint, uint16_t dist){
   
//float factor = velMax/dist;
  for(int i=dist; i>=0 ; i = i-5){
    //float velF = i*factor;
    float velF = 0.05f;
    if(i<=0){velF = 0;}
    
    setHoverSetpoint(setpoint, velF, 0, height, 0);
    commanderSetSetpoint(setpoint, 3);
    vTaskDelay(M2T(20));
  }

}



void setLeds(int ledNumber){
    ledNumber = ledNumber;
    if(ledNumber == 5) {

        ledSetAll();
      }
      else if(ledNumber == 6) {
        ledClearAll();
      }
      else {

      ledSet(ledNumber, true);
    }
}


#endif