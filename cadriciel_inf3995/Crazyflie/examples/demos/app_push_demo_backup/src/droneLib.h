#ifndef __DRONELIB_H__
#define __DRONELIB_H__

#include "app.h"
#include "led.h"

typedef enum {
    idle,
    lowUnlock,
    unlocked,
    stopping,
    forward,
    moveback,
    rotateRight,
    rotateLeft,
    lowTakeOff,
    takeOff,
    findWall,
    lost,
    lostRotateLeft,
    crashed,
    returnToBase,
    returnAngles,
    returnForward,
    returnRightLeft

} State;

/************************ Code Serveur **********************/
typedef enum {STANDBY = 0, IN_MISSION, CRASHED } droneState;


struct testPacketRX {
  int LedsOn;
  int command;
} __attribute__((packed));

struct rangingPacketTX {
  float front;
  float back;
  float left;
  float right;
  float up;
  float parity;
} __attribute__((packed));

struct droneDatasTX {
  float state;
  float battery;
  float speed;
  float LedsOn;
  float droneNumber;
  float parity;
} __attribute__((packed));

struct sensorPositionFB {

  /*float frontPosX;
  float frontPosY;
  float backPosX;
  float backPosY;
  float backPosYF;*/

  float posx;
  float posy;

  float Rposx;
  float Rposy;

  float angle; // matrice ou yaw  a voir 
  float parity;
} __attribute__((packed));

struct sensorPositionLR {
  
  float leftPosX;
  float leftPosY;
  float rightPosX;
  float rightPosY;
  float rightPosYF;

  float parity;
} __attribute__((packed));


/*********************************************************/

#define MAX(a,b) ((a>b)?a:b)
#define MIN(a,b) ((a<b)?a:b)

static const uint16_t unlockThLow = 100;
static const uint16_t unlockThHigh = 300;
static const uint16_t stoppedTh =500;

static const float velMax = 1.0f;
static const uint16_t radius = 300;
static const uint16_t lowRadius = 200;

static const float height_sp = 0.2f;

static const float stepX = 0.4f;
static const float stepY = 0.4f;
static const float angle = 15.0f;

static const float tabAngle[12] = {45,-45,60,-60,90,-90,120,-120,150,-150,180,-180};

static float height;
static float velFront = 0.0f; 
static float velSide = 0.0f;

static float anglePos[9];

static float ledNumber = 6.0f;

uint8_t message;


//static struct rangingPacketTX txPacketRanging;


//static float Message;
//struct rangingPacketTX txPacket2Ranging;
//struct sensorPositionFB txDrone2Pos;
//struct droneDatasTX drone2Data;


// Fonction pour faire avancer le drone

#define MESSAGE "hello world"
#define MESSAGE_LENGHT 11




static void setHoverSetpoint(setpoint_t *setpoint, float vx, float vy, float z, float yawrate)
{
  setpoint->mode.z = modeAbs;
  setpoint->position.z = z;


  setpoint->mode.yaw = modeVelocity;
  setpoint->attitudeRate.yaw = yawrate;


  setpoint->mode.x = modeVelocity;
  setpoint->mode.y = modeVelocity;
  setpoint->velocity.x = vx;
  setpoint->velocity.y = vy;

  setpoint->velocity_body = true;
}

void moveLeft(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, velFront, stepY, height, 0);
    commanderSetSetpoint(setpoint, 3);
}

void moveRight(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, velFront, -stepY, height, 0);
    commanderSetSetpoint(setpoint, 3);
}

void turnRight(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, velFront, velSide, height, -angle);
    commanderSetSetpoint(setpoint, 3);
}

void turnLeft(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, velFront, velSide, height, angle);
    commanderSetSetpoint(setpoint, 3);
    //vTaskDelay(M2T(20));
}

void moveForward(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, stepX, velSide, height, 0);
    commanderSetSetpoint(setpoint, 3);
    //vTaskDelay(M2T(20));
}

void rotateWithAngle(setpoint_t *setpoint, float rotateAngle){
  if(rotateAngle >= 0){
    for (int i = 15; i <= rotateAngle ; i = i+ 15 ) { 
      setHoverSetpoint(setpoint, velFront, velSide, height, rotateAngle);
      commanderSetSetpoint(setpoint, 3);
      //vTaskDelay(M2T(20));
    }
  }else{
    for (int i = -15; i >= -rotateAngle ; i = i - 15 ) { 
      setHoverSetpoint(setpoint, velFront, velSide, height, rotateAngle);
      commanderSetSetpoint(setpoint, 3);
      //vTaskDelay(M2T(20));
    }
  }
    
}

void moveInit(setpoint_t *setpoint){
    setHoverSetpoint(setpoint, 0, 0, height, 0);
    commanderSetSetpoint(setpoint, 3);
    //vTaskDelay(M2T(20));
}

void moveTo(setpoint_t *setpoint, uint16_t dist){
   
//float factor = velMax/dist;
  for(int i=dist; i>=0 ; i = i-5){
    //float velF = i*factor;
    float velF = 0.05f;
    if(i<=0){velF = 0;}
    
    setHoverSetpoint(setpoint, velF, 0, height, 0);
    commanderSetSetpoint(setpoint, 3);
    vTaskDelay(M2T(20));
  }

}



void setLeds(int ledNumber){
    ledNumber = ledNumber;
    if(ledNumber == 5) {

        ledSetAll();
      }
      else if(ledNumber == 6) {
        ledClearAll();
      }
      else {

      ledSet(ledNumber, true);
    }
}


void p2pcallbackHandler(P2PPacket *p)
{
  // Parse the data from the other crazyflie and print it
 // uint8_t other_id = p->data[0];
  //static char msg[MESSAGE_LENGHT + 1];
 // memcpy(&msg, &p->data[1], sizeof(Message));
  

 /* txDrone2Pos.posx = (float)p->data[1];
  txDrone2Pos.posy = p->data[5];
  txDrone2Pos.Rposx = p->data[9];
  txDrone2Pos.Rposy = (float)p->data[13];
  txDrone2Pos.angle = p->data[17];
  txDrone2Pos.parity=4.0f;

  txPacket2Ranging.front = p->data[21];
  txPacket2Ranging.back = p->data[25];
  txPacket2Ranging.left = p->data[29];
  txPacket2Ranging.right = p->data[33];
  txPacket2Ranging.parity=5.0f;*/

 /* drone2Data.state = p->data[10];
  drone2Data.battery = p->data[11];
  drone2Data.speed = p->data[12];
  drone2Data.LedsOn = 6.0f;
  drone2Data.droneNumber = 20.0f;
  drone2Data.parity=6.0f;*/

  // msg[MESSAGE_LENGHT] = 0;
  // uint8_t rssi = p->rssi;

  // DEBUG_PRINT("[RSSI: -%d dBm] Message from CF nr. %d, %f\n", rssi, other_id, msg);

  DEBUG_PRINT("[RSSIIIIIIIIIIIIIIIII: dBm] Message from CF %d \n",message);
}

#endif