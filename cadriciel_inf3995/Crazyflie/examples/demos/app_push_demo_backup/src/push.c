#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "app.h"
#include "led.h"


#include "radiolink.h"
#include "configblock.h"

#include "commander.h"

#include "FreeRTOS.h"
#include "task.h"

#include "debug.h"

#include "log.h"
#include "param.h"
#include "droneLib.h"

#include "app_channel.h"

#include "estimator_kalman.h"

#include "pm.h"
#include "sitaw.h"




#define DEBUG_MODULE "PUSH"

static State state = idle;

bool idlePass = false;

// wall detection  variables
bool detectLeft = false;
bool detectRight = false;
bool detectFront = false;
bool detectBack = false;
//bool detectUp = false;

bool turn = false;
bool droneTakeoff = false;
int position;

float stateDrone = 0.0f;

float returnAngle = 0.0f;

float velX;
float velY;



static logVarId_t logIdStateEstimateX;
static logVarId_t logIdStateEstimateY;





static float getX() { return logGetFloat(logIdStateEstimateX); }
static float getY() { return logGetFloat(logIdStateEstimateY); }



void appMain()
{
  
  DEBUG_PRINT("Waiting for activation ...\n");

  static setpoint_t setpoint;
  static point_t setPosition;

/*********** Code Serveur ***************/
   
struct droneDatasTX txPacketDatas;
struct rangingPacketTX txPacketRanging;
struct testPacketRX rxPacket; 

struct sensorPositionFB txDrone1Pos;
//struct sensorPositionFB positionFB;
//struct sensorPositionLR positionLR;
/*********** *********** ***************/

/* Initialize the p2p packet 
    static P2PPacket p_reply;
    p_reply.port=0x00;
    
    // Get the current address of the crazyflie and obtain
    //   the last two digits and send it as the first byte
    //   of the payload
    uint64_t address = configblockGetRadioAddress();
    uint8_t my_id =(uint8_t)((address) & 0x00000000ff);
    p_reply.data[0]= my_id;

    //Put a string in the payload
    //char *str="Hello World";
    float messageSend = 20.0;
    //memcpy(&p_reply.data[1], messageSend, sizeof(messageSend));

    // Set the size, which is the amount of bytes the payload with ID and the string 
    p_reply.size=(sizeof(messageSend)*13) + 1;

    // Register the callback function so that the CF can receive packets as well.
    p2pRegisterCB(p2pcallbackHandler);

*************************************************/



  logVarId_t idUp = logGetVarId("range", "up");
  logVarId_t idLeft = logGetVarId("range", "left");
  logVarId_t idRight = logGetVarId("range", "right");
  logVarId_t idFront = logGetVarId("range", "front");
  logVarId_t idBack = logGetVarId("range", "back");
  logVarId_t idYaw = logGetVarId("stateEstimate", "yaw");


  logIdStateEstimateX = logGetVarId("stateEstimate", "x");
  logIdStateEstimateY = logGetVarId("stateEstimate", "y");



  //paramVarId_t idPositioningDeck = paramGetVarId("deck", "bcFlow2");
  paramVarId_t idMultiranger = paramGetVarId("deck", "bcMultiranger");



  float factor = velMax/lowRadius;

  uint16_t up_o;

  float yaw = 0.0f;
  float getx = 0.0f;

  while(1)
  {
    vTaskDelay(M2T(10));
    
    uint16_t up = logGetUint(idUp);
    uint16_t left = logGetUint(idLeft);
    uint16_t right = logGetUint(idRight);
    uint16_t front = logGetUint(idFront);
    uint16_t back = logGetUint(idBack);


    // float yaw = messageSend;
    yaw = logGetFloat(idYaw);
    getx = logGetFloat(logIdStateEstimateX);
    // DEBUG_PRINT("Yaw is now: %f deg\n", (double)yaw);
    DEBUG_PRINT("GETX is now: %f deg\n", (double)getx);

   // uint8_t positioningInit = paramGetUint(idPositioningDeck);
    uint8_t multirangerInit = paramGetUint(idMultiranger);
  


   if( multirangerInit /*&& positioningInit*/) {

      txPacketRanging.front=  front; 
      txPacketRanging.back = back;
      txPacketRanging.left = left;
      txPacketRanging.right= right;
      txPacketRanging.up = up;
      txPacketRanging.parity= 0.0f;

      txPacketDatas.state = stateDrone;
      txPacketDatas.battery = pmGetBatteryVoltage();
      txPacketDatas.speed = sqrt((stepX * stepX) + (stepY* stepY));
      txPacketDatas.LedsOn = ledNumber;
      txPacketDatas.droneNumber = -1;
      txPacketDatas.parity= 1.0f;

      /*txDrone1Pos.posx = setPosition.x ; //1000.0f;
      txDrone1Pos.posx = setPosition.y; //  1000.0f;
      txDrone1Pos.angle0 = getX(); //anglePos[0];
      txDrone1Pos.angle3 = getY();
      txDrone1Pos.angle = getVarPX();     //yaw;
      txDrone1Pos.parity = 7;*/

      estimatorKalmanGetEstimatedRot(anglePos);
      estimatorKalmanGetEstimatedPos(&setPosition);

      
      DEBUG_PRINT("Gety is now: %f deg\n", (double)getY());
      DEBUG_PRINT("Posx is now: %f deg\n", (double)setPosition.x);
      DEBUG_PRINT("Posy is now: %f deg\n", (double)setPosition.y);

      txDrone1Pos.posx =  getX();
      txDrone1Pos.posy =  getY();
      txDrone1Pos.Rposx = anglePos[0];
      txDrone1Pos.Rposy = anglePos[3];
      txDrone1Pos.angle = yaw;
      txDrone1Pos.parity = 7.0f;

      

   
   //memcpy(&p_reply.data[1], getX(), sizeof(getX()))
   //memcpy(&p_reply.data[5], getX(), sizeof(getX()))
   // messageSend = (uint8_t)getX();

   /* p_reply.data[1] = messageSend; //getX(); //setPosition.x; / 1000.0f;
    p_reply.data[5] = messageSend; //getY(); //setPosition.y; / 1000.0f;
    p_reply.data[9] = messageSend; //setPosition.x; //getX();
    p_reply.data[13] = getX(); //setPosition.y;//getY();
    p_reply.data[17] = getX(); //yaw

    message = getX();
   

    p_reply.data[21] = front;
    p_reply.data[25] = back;
    p_reply.data[29] = left;
    p_reply.data[33] = right;

    p_reply.data[10] = messageSend; //stateDrone;
    p_reply.data[11] = pmGetBatteryVoltage();
    p_reply.data[12] = sqrt((stepX * stepX) + (stepY* stepY));
    //p_reply.data[11] = ledNumber;


      positionFB.frontPosX = setPosition.x*1000 + (float)front * (anglePos[0]);
      positionFB.frontPosY = setPosition.y*1000 + (float)front * (anglePos[3]);

      positionFB.backPosX = setPosition.x + (float)back * ((anglePos[0] * (float)cos(180)) - (anglePos[3] * (float)sin(180)));
      positionFB.backPosY = setPosition.y + (float)back * ((anglePos[3] * (float)cos(180)) + (anglePos[0] * (float)sin(180)));
      positionFB.backPosYF = setPosition.y + (float)back * ((anglePos[3] * (float)cos(180)) + (anglePos[0] * (float)sin(180)));

      positionFB.parity = 2;

      positionLR.leftPosX = setPosition.x + (float)left * ((anglePos[0] * (float)cos(90)) - (anglePos[3] * (float)sin(90)));
      positionLR.leftPosY = setPosition.y + (float)left * ((anglePos[3] * (float)cos(90)) + (anglePos[0] * (float)sin(90)));

      positionLR.rightPosX = setPosition.x + (float)right * ((anglePos[0] * (float)cos(270)) - (anglePos[3] * (float)sin(270)));
      positionLR.rightPosY = setPosition.y + (float)right * ((anglePos[3] * (float)cos(270)) + (anglePos[0] * (float)sin(270)));
      positionLR.rightPosYF = setPosition.y + (float)right * ((anglePos[3] * (float)cos(270)) + (anglePos[0] * (float)sin(270)));
    
      positionLR.parity = 3;*/

    }

    appchannelSendPacket(&txPacketRanging, sizeof(txPacketRanging));
    appchannelSendPacket(&txDrone1Pos, sizeof(txDrone1Pos)); 
    appchannelSendPacket(&txPacketDatas, sizeof(txPacketDatas)); 
   // appchannelSendPacket(&positionFB, sizeof(positionFB)); 
    //appchannelSendPacket(&positionLR, sizeof(positionLR));

    //appchannelSendPacket(&txPacket2Ranging, sizeof(txPacket2Ranging));
    //appchannelSendPacket(&txDrone2Pos, sizeof(txDrone2Pos)); 
    //appchannelSendPacket(&drone2Data, sizeof(drone2Data)); 
    
   
    // Send a message every 2 seconds
    //   Note: if they are sending at the exact same time, there will be message collisions, 
    //    however since they are sending every 2 seconds, and they are not started up at the same
    //    time and their internal clocks are different, there is not really something to worry about

    //vTaskDelay(M2T(1000));
    //radiolinkSendP2PPacketBroadcast(&p_reply);



   
   
/***************************************************/

    if (appchannelReceivePacket(&rxPacket, sizeof(rxPacket), 0)) {
      //ledSetAll();
      //vTaskDelay(M2T(5000));
      DEBUG_PRINT("App channel received Set Led ON Led: %d\n", rxPacket.LedsOn);
      int led = rxPacket.LedsOn;
      int command = rxPacket.command;
      //setLeds(6);
      //vTaskDelay(M2T(2000));
      // appeller la machine a etat avec command
      /*if(command == 2){
          txPacketRanging.front = 10000.0;
          appchannelSendPacket(&txPacketRanging, sizeof(txPacketRanging)); 
          droneTakeoff = true;
    
      }*/
      switch(command){
        case 0:
          setLeds(led);
          break;
        case 1:
          setLeds(5);
          txPacketRanging.front = 10000.0;
          appchannelSendPacket(&txPacketRanging, sizeof(txPacketRanging)); 
          droneTakeoff = true;
          break;
        case 2: // UPDATE
          break;
        case 3: // LAND
          state = stopping ;
          break;
        case 4: // return to base
          state = returnToBase;
          break;
      }
      
    }
 // DEBUT DU CODE DE TAKEOFF
   
   uint16_t left_o = lowRadius - MIN(left, lowRadius);
   uint16_t right_o = lowRadius - MIN(right, lowRadius);
   float l_comp = (-1) * left_o * factor;
   float r_comp = right_o * factor;
   velSide = r_comp + l_comp;
  

   uint16_t front_o = lowRadius - MIN(front, lowRadius);
   uint16_t back_o = lowRadius - MIN(back, lowRadius);
   float f_comp = (-1) * front_o * factor;
   float b_comp = back_o * factor;
   velFront = b_comp + f_comp;
  

   
    detectLeft = (left < radius);
    detectRight = (right < radius);
    detectFront = (front < radius);
    detectBack = (back < radius);

    up_o = radius - MIN(up, radius);
    height = height_sp - up_o/1000.0f;

    if (up < unlockThLow && up > 0.001f && idlePass) {
        DEBUG_PRINT("Waiting for hand to be removed!\n");
        idlePass = false;
        state = stopping ;
    }
  
    if(sitAwTuDetected()){
      state = crashed;
    }

    switch (state)
    {
    case idle:
      memset(&setpoint, 0, sizeof(setpoint_t));
      commanderSetSetpoint(&setpoint, 3);
      //if (up < unlockThLow && up > 0.001f) {
        if (droneTakeoff) {
        DEBUG_PRINT("Waiting for hand to be removed!\n");
        //state = lowTakeOff;
        state = takeOff;
      }
      
      break;

    case lowTakeOff:
       if (up > unlockThHigh) {
           DEBUG_PRINT("Unlocked!\n");
            idlePass = true;
           state = takeOff;
        }
        
    break;

    case takeOff :
     
      // setHoverSetpoint(&setpoint, 0, 0, height,0);
      //commanderSetSetpoint(&setpoint, 3);
      stateDrone = 1.0f;
      state = findWall;
      
    break;
    case stopping :
       memset(&setpoint, 0, sizeof(setpoint_t));
       commanderSetSetpoint(&setpoint, 3);
       droneTakeoff = false;
       state = idle;
    break; 

    // trip startpoint
    case findWall:
      if(!detectLeft){
          moveLeft(&setpoint);
      }else{
        if(detectLeft){
          moveInit(&setpoint);
         // moveLeft(&setpoint);
          state = forward;
         //ledClearAll();
         
          //vTaskDelay(M2T(2000));
          
        }
        
      } 
     
    break;

    case forward:
    if(!detectFront && (detectLeft || detectRight) ){
        moveForward(&setpoint);
      }else{

        if(detectFront){
          moveInit(&setpoint);
          state = rotateRight;
        }else{

          if(!detectFront && (!detectLeft || !detectRight )){
            //state = stopping;
            moveInit(&setpoint);
            state = lost;
          }
          else{
            state = lost;
          }
        }
        
      }
    
    //foundwall = false;
    //turn = false;
    break;

    case rotateRight :
   // turnRight(&setpoint);
    position = rand()%12;
    rotateWithAngle(&setpoint, tabAngle[position]); 
    /*if(!detectFront){
        moveInit(&setpoint);
        state = forward;
       //state = findWall;
    }*/
    state = findWall;
   
    break;

    case lost :
     //moveTo(&setpoint, 100);
     //vTaskDelay(M2T(20));
     //rotateWithAngle(&setpoint, 65.0f);  
     // vTaskDelay(M2T(20));
     //moveInit(&setpoint);
      //state = findWall;
      position = rand()%12;
      rotateWithAngle(&setpoint, tabAngle[position]);  
      // vTaskDelay(M2T(50));
      state = findWall;

    break;
    
    case crashed :
      stateDrone = 2.0f;
      state = stopping;

    break;

     case returnToBase :
      velX = (setPosition.x < -0.1f)? stepX : (setPosition.x  > 0.1f)? -stepX : 0.0f;
      velY = (setPosition.y < -0.1f)? stepY : (setPosition.x  > 0.1f)? -stepY : 0.0f;
     
      if( (velX == 0.0f) & (velY == 0.0f)){
        state = stopping;
      }

      returnAngle = (float)atan(velY/velX);
      ledSetAll();
      state = returnAngles;  
      break;

      case returnAngles:
        rotateWithAngle(&setpoint, returnAngle);
        state = returnForward;
      break;

       case returnForward:
         if(!detectFront){
            moveForward(&setpoint);
         }else{
            state = returnRightLeft;
         }
         
        break;

        case returnRightLeft:
           if(detectFront){
              
              if(detectRight < detectLeft){
                  moveLeft(&setpoint);
              }else{
                  moveRight(&setpoint);
              }
           }else{
             state = returnToBase;
           }
        break;

    default:
      break;
    }

  }
}